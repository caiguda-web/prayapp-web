var express = require('express');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var	expressSession = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var config = require('config');
var logger = require('./libs/logger');

// register jobs in agenda
require('./jobs/jobs');

// routers
var authorization = require('./routes/authorization');
var managers = require('./routes/managers');
var locations = require('./routes/locations');
var locationsMap = require('./routes/locationsMap');

var reports = require('./routes/reports');
var restore = require('./routes/restore');

// API routers
var apiPrayerLocations = require('./routes/api/prayerLocations');
var apiAdmins = require('./routes/api/admins');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/favicon.ico'));


// logging
// ensure log directory exists 
// var logStream = fs.createWriteStream(logDirectory + '/main.log', {flags: 'a'})
// app.use(logger('combined', {stream: logStream}))
// app.use(morgan('dev'));


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./middleware/normSend'));

//
app.use(expressSession({
	name: "prayapp-session",
	secret: "prayapp-secretKey",
	resave: false,
	saveUninitialized: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// Views
app.use('/', authorization);
app.use('/managers', managers);
app.use('/locations', locations);
app.use('/locationsMap', locationsMap);
app.use('/reports', reports);
app.use('/restore', restore);

// API (AJAX)
app.use('/v1/locations', apiPrayerLocations);
app.use('/v1/admins', apiAdmins);

app.use('/*', authorization);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var error = new Error('Page not found');
    error.status = 404;
    next(error);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(error, req, res, next) {
			console.log('ERROR HANDLER !!!!!', error);
			// var result = {code: err.status || 500, data: null, error: {message: error.message, details: error}};
			res.send(error.message);			
    });
}

var port = config.get("application.cmsPort");
app.listen(port);
logger.info("App started on port: %s, environment: %s", port, config.util.getEnv('NODE_ENV'));

module.exports = app;
