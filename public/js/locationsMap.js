var map = null;
var infoWindow = null;

function initMap() {
	var mapOptions = {
		zoom: 1
	};
	map = new google.maps.Map(document.getElementById('map'), mapOptions);
	var centerCoord = new google.maps.LatLng(0, 0);
	map.setCenter(centerCoord);
	
	$.get('/v1/locations/all-for-map', function(response) {
		if(response.data)
			setupMarkers(response.data);
	});
}

function setupMarkers(locations) {
	if(!locations)
		return;
	
	var markers = locations.map(function(loc) {
		var marker = new google.maps.Marker({
			position: {lat: loc.location.coordinate[1], lng: loc.location.coordinate[0]}
		});
		marker.prayerLocation = loc;
		google.maps.event.addListener(marker, 'click', function() {
			showInfoWindow(marker);
		});
		return marker;
	});

	var markerCluster = new MarkerClusterer(map, markers, {imagePath: '/markerclusterer/images/m'});
}

function showInfoWindow(marker) {
	if(!marker || !marker.prayerLocation)
		return;
	
	var prayerLoc = marker.prayerLocation;
	var content = "<p><b>" + prayerLoc.name + "</b></p><p>" + prayerLoc.location.address + "</p>";
	content += "<div><a href='/locations/location/" + prayerLoc._id + "'>Edit</a></div>"
	if(!infoWindow)
		infoWindow = new google.maps.InfoWindow({content: content});
	else
	{
		infoWindow.close();
		infoWindow.setContent(content);		
	}
	infoWindow.open(map, marker);
}
