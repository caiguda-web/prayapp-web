var modalSpinner = null;

function showModal() {
	var modalTarget = document.getElementById('modalSpinner')
	modalSpinner = new Spinner({lines: 9, scale: 1, hwaccel: true, top: '40%', left: '50%'}).spin(modalTarget);
	$('#progressText').addClass('visible');
	$('#savedText').addClass('hidden');
  $('#entityResultModal').modal('show');	
}

function hideModal(success) {
	if(success) {
		$('#progressText').removeClass('visible');
		$('#progressText').addClass('hidden');
		$('#savedText').removeClass('hidden');
		$('#savedText').addClass('visible');
		setTimeout(function(){
			if(modalSpinner) {
				modalSpinner.stop();
				modalSpinner = null;
			}
		  $('#entityResultModal').modal('hide');		
		}, 600);		
	}
	else {
		if(modalSpinner) {
			modalSpinner.stop();
			modalSpinner = null;
		}
	  $('#entityResultModal').modal('hide');		
	}
}
