var map = null;
var marker = null;
var infoWindow = null;

function initMap() {
	var mapOptions = {
		zoom: 16
	};
	map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var locationCoord;

	if(spot.location.coordinate)
	{
		locationCoord = new google.maps.LatLng(spot.location.coordinate.lat, spot.location.coordinate.lng);
		setupLocationCenter(true, locationCoord);
	}
}

function configureInfoWindow() {
	var address = spot.location.address ? spot.location.address : "";
	var content = "<p><b>Spot</b></p><p>" + address + "</p>";
	if(!infoWindow)
		infoWindow = new google.maps.InfoWindow({content: content});
	else
		infoWindow.setContent(content);	
}

function setupLocationCenter(locationDetected, centerCoord) {
	map.setCenter(centerCoord);
	map.setZoom(16);		

	configureInfoWindow();

	marker = new google.maps.Marker({
		position: centerCoord,
		draggable: false
	});
	marker.setMap(map);
	
	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.open(map, marker);
	});
}

$(document).ready(function() {
});
