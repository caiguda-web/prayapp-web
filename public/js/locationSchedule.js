$('#scheduleForm').on('submit', function(event) {
	event.preventDefault();

	// var target = document.getElementById('scheduleSubmit')
	// var spinner = new Spinner({lines: 9, scale: 0.5, hwaccel: true, top: '50%', left: '90%'}).spin(target);

	showModal();
	$.ajax({
		type: 'POST',
		url: '/v1/locations/' + $('#id').val() + '/prayertimes',
		data: $(this).serialize(),
		error: function(jq, error, message) {
			hideModal(false);
			alert(message);
		},
		success: function(response) {
			hideModal(!response.error);
		}
	});	
});


// http://praytimes.org/wiki/Code_Manual

if(prayerLocationGeneral)
{
	prayTimes.setMethod('ISNA');
	var times = prayTimes.getTimes(new Date(), [prayerLocationGeneral.lat, prayerLocationGeneral.lng], 'auto', 'auto', '12h');

	$('#calckMethodInfo').html('&#42;to calculate pray times ISNA method was used');
	$('#timeFajrStartHelp').html('Today Fajr time is: ' + times.fajr + '&#42;');
	$('#timeDhuhrStartHelp').html('Today Dhuhr time is: ' + times.dhuhr + '&#42;');
	$('#timeAsrStartHelp').html('Today Asr time is: ' + times.asr + '&#42;');
	$('#timeMaghribStartHelp').html('Today Maghrib time is: ' + times.maghrib + '&#42;');
	$('#timeIshaStartHelp').html('Today Isha time is: ' + times.isha + '&#42;');	
}
