var map = null;
var marker = null;
var infoWindow = null;
var currMarkerPosition;

function initMap() {
	var mapOptions = {
		zoom: 16
	};
	map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var locationCoord;

	if(prayerLocationGeneral)
	{
		locationCoord = new google.maps.LatLng(prayerLocationGeneral.lat, prayerLocationGeneral.lng);
		setupLocationCenter(true, locationCoord);
	}
	else
	{
		if(navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition(function(position) {
				locationCoord = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				setupLocationCenter(true, locationCoord);
			},
			function() {	// handle geolocation error
				locationCoord = new google.maps.LatLng(0, 0);
				setupLocationCenter(false, locationCoord);
			});
		}
		else	// browser doesn't support geolocation
		{
			locationCoord = new google.maps.LatLng(0, 0);
			setupLocationCenter(false, locationCoord);
		}
	}
}

function configureInfoWindow() {
	var name = prayerLocationGeneral ? prayerLocationGeneral.name : "New Location";
	// var address = prayerLocationGeneral ? prayerLocationGeneral.address : "";
	var address = $("#searchAddress").val();
	var content = "<p><b>" + name + "</b></p><p>" + address + "</p>";

	if(!infoWindow)
		infoWindow = new google.maps.InfoWindow({content: content});
	else
		infoWindow.setContent(content);	
}

function setupLocationCenter(locationDetected, centerCoord) {
	map.setCenter(centerCoord);
	if(!locationDetected)
	{
		map.setZoom(3);
	}
	else
	{
		map.setZoom(16);		
	}
	
	configureInfoWindow();

	marker = new google.maps.Marker({
		position: centerCoord,
		draggable: true,
		title: ''//markerTitle
	});
	marker.setMap(map);
	currMarkerPosition = centerCoord;
	if(locationDetected)
	{
		geocodePosition(centerCoord);		
	}
	
	google.maps.event.addListener(marker, 'click', function() {
		infoWindow.open(map, marker);
	});
	google.maps.event.addListener(marker, 'dragstart', function() {
		infoWindow.close();
	});
	google.maps.event.addListener(marker, 'dragend', function(event) {
		geocodePosition(event.latLng);
	});
	google.maps.event.addListener(map, 'dblclick', function(event) {
		infoWindow.close();
		marker.setPosition(event.latLng);
		geocodePosition(event.latLng);
	});
	
	configureAutocomplete();
}

function configureAutocomplete() {
	var searchAddressField = document.getElementById('searchAddress');
	var options = {
		types: ['address']
	};
	
	var autocomplete = new google.maps.places.Autocomplete(searchAddressField, options);
	autocomplete.bindTo('bounds', map);
	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
		if(!place.geometry)
		{
			alert("No details available for current search address");
			return;
		}

		infoWindow.close();
		marker.setVisible(false);
		if(place.geometry.viewport)
		{
			map.fitBounds(place.geometry.viewport);
		}
		else
		{
			map.setCenter(place.geometry.location);
			map.setZoom(16);
		}
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);
		configureInfoWindow();
	});
}

function geocodePosition(latLng) {
	var geocoder = new google.maps.Geocoder();
	var options = {location: latLng};
	geocoder.geocode(options, function(results, status) {
		if(status == google.maps.GeocoderStatus.OK)
		{
			currMarkerPosition = new google.maps.LatLng(latLng.lat(), latLng.lng());
			
			$("#searchAddress").val(results[0].formatted_address);			
			configureInfoWindow();
		}
		else
		{
			marker.setPosition(currMarkerPosition);
			map.setCenter(currMarkerPosition);
			alert("Can't determine address of selected location. " + status);
		}
	});
}

$('#locationForm').validator().on('submit', function(event) {
	if(event.isDefaultPrevented())
		return;
	
	event.preventDefault();
	
	// console.log($('#locationForm').prop('submiting'));
	if($('#locationForm').prop('submiting') == true)
		return;
	
	$('#locationForm').prop('submiting', true);
	
	var requestData = {
		name: $("#name").val(),
		placeType: $("#placeType").val(),
		timeOpen: $("#timeOpen").val(),
		timeClose: $("#timeClose").val(),
		alwaysOpen: $("#alwaysOpen").prop('checked'),
		latitude: Number(marker.position.lat()),
		longitude: Number(marker.position.lng()),
		address: $("#searchAddress").val(),
		managerUsername: $("#manager").val()
	};
	if($("#id").val())
	{
		requestData.id = $("#id").val();
	}
	
	// var target = document.getElementById('saveGeneralSubmit')
	// var spinner = new Spinner({lines: 9, scale: 0.5, hwaccel: true, top: '40%', left: '90%'}).spin(target);
	$('#saveGeneralSubmit').addClass('disabled');
	$('#saveGeneralSubmit').addClass('disableClick');

	showModal();

	if(requestData.id)
	{
		$.ajax({
			url: '/v1/locations/' + requestData.id + '/general',
			method: 'POST',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			data: JSON.stringify(requestData),
			error: function(req, errorType, message) {
				hideModal(false);
				alert(message);
			},
			success: function(res) {
				hideModal(!res.error);
				if(res.error)
				{
					alert(res.error.message);
				}
			},
			complete: function(req, status) {
				// hideModal();
				// spinner.stop();
				$('#saveGeneralSubmit').removeClass('disabled');
				$('#saveGeneralSubmit').removeClass('disableClick');
				$('#locationForm').prop('submiting', false);				
			}
		});
	}
	else
	{
		prayTimes.setMethod('ISNA');
		var times = prayTimes.getTimes(new Date(), [requestData.latitude, requestData.longitude]);
		
		requestData.timeFajrStart = moment(times.fajr, 'HH:mm').format('hh:mm A');
		requestData.timeDhuhrStart = moment(times.dhuhr, 'HH:mm').format('hh:mm A');
		requestData.timeAsrStart = moment(times.asr, 'HH:mm').format('hh:mm A');
		requestData.timeMaghribStart = moment(times.maghrib, 'HH:mm').format('hh:mm A');
		requestData.timeIshaStart = moment(times.isha, 'HH:mm').format('hh:mm A');
		$.ajax({
			url: '/v1/locations',
			method: 'POST',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			data: JSON.stringify(requestData),
			error: function(req, errorType, message) {
				hideModal(false);
				alert(message);
			},
			success: function(res) {
				hideModal(!res.error);
				if(res.error)
				{
					alert(res.error.message);
				}
				else
				{
					location.replace('/locations/location/' + res.data);
				}
			},
			complete: function(req, status) {
				// spinner.stop();
				$('#saveGeneralSubmit').removeClass('disabled');
				$('#saveGeneralSubmit').removeClass('disableClick');
				$('#locationForm').prop('submiting', false);
			}
		});
	}
});


$(document).ready(function() {
	
	// showModal();
	// setTimeout(function() {
	// 	hideModal();
	// }, 5000);

	if(adminRole == 'master') {
		$('#manager').selectize({
			create: false,
			valueField: 'username',
			labelField: 'username',
			searchField: 'username',
		  create: false,
			preload: true,
		  render: {
		  	option: function(item, escape) {
					// alert(JSON.stringify(item));
		  		return '<div>' + escape(item.username) + '</div>';
		  	}
		  },
			load: function(query, cb) {
				// if (!query.length) return cb();
				var url = null;
				if(query.length)
					url = '/v1/admins/usernamemany/' + encodeURIComponent(query);
				else
					url = '/v1/admins/allmanagers'
				$.ajax({
					url: url,
					type: 'GET',
					error: function() {
						cb();
					},
					success: function(res) {
						// alert(JSON.stringify(res));
						cb(res.data);
					}
				});						
			}
		});
		$('#locationForm').validator('update');
	}	
});

$('#alwaysOpen').on('change', function(event) {
	if($(this).prop('checked'))
	{
		$("#openCloseTimeSet").prop('disabled', 'disabled');
		$("#timeOpen").val('');
		$("#timeClose").val('');
	}
	else
	{
		$("#openCloseTimeSet").prop('disabled', null);		
	}
});