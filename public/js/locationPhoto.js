function configureFilesUploader(objectQuery, maxFiles, uploadUrl, imagesUrl, successFunc) {
	// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
	var previewNode = document.querySelector(objectQuery + " .template");
	previewNode.id = "";
	var previewTemplate = previewNode.parentNode.innerHTML;
	previewNode.parentNode.removeChild(previewNode);

	var myDropzone = new Dropzone(objectQuery, {
		paramName: 'photo',
	  url: uploadUrl,
		maxFileSize: 3,	// MB
		maxFiles: maxFiles,
		acceptedFiles: 'image/*',
	  thumbnailWidth: 40,
	  thumbnailHeight: 40,
	  parallelUploads: 1,
	  previewTemplate: previewTemplate,
	  autoQueue: false, // Make sure the files aren't queued until manually added
	  previewsContainer: objectQuery + " .previews", // Define the container to display the previews
	  clickable: objectQuery + " .fileinput-button" // Define the element that should be used as click trigger to select files.
	});

	myDropzone.on("addedfile", function(file) {
	  // Hookup the start button
	  file.previewElement.querySelector(objectQuery + " .start").onclick = function() { myDropzone.enqueueFile(file); };
		document.querySelector(objectQuery + " .dragHereLabel").style.display = 'none';
	});
	
	myDropzone.on("reset", function() {
		document.querySelector(objectQuery + " .dragHereLabel").style.display = 'block';
	});

	// Update the total progress bar
	myDropzone.on("totaluploadprogress", function(progress) {
	  document.querySelector(objectQuery + " .total-progress .progress-bar").style.width = progress + "%";
	});

	myDropzone.on("sending", function(file) {
	  // Show the total progress bar when upload starts
	  document.querySelector(objectQuery + " .total-progress").style.opacity = "1";
	  // And disable the start button
	  file.previewElement.querySelector(objectQuery + " .start").setAttribute("disabled", "disabled");
	});

	// Hide the total progress bar when nothing's uploading anymore
	myDropzone.on("queuecomplete", function(progress) {
	  document.querySelector(objectQuery + " .total-progress").style.opacity = "0";
		document.querySelector(objectQuery + " .dragHereLabel").style.display = 'block';
	  myDropzone.removeAllFiles(true);
		$.get(imagesUrl, function(response) {
			//alert(JSON.stringify(response));
			if(response.error)
			{
				alert(response.error.message);
			}
			else
			{
				successFunc(response);
			}
		});
	});

	myDropzone.on("complete", function(file) {
	  myDropzone.removeFile(file);
	});

	// Setup the buttons for all transfers
	// The "add files" button doesn't need to be setup because the config
	// `clickable` has already been specified.
	document.querySelector(objectQuery + " .actions .start").onclick = function() {
	  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
	};
	document.querySelector(objectQuery + " .actions .cancel").onclick = function() {
	  myDropzone.removeAllFiles(true);
	};
}

function tryToDeleteMainImage(imageName) {
	var del = confirm("Are you sure?");
	if(del)
	{
		$.ajax({
			type: 'DELETE',
			url: '/v1/locations/' + $('#id').val() + '/photo',
			error: function(jq, error, message) {
				alert(message);
			},
			success: function(response) {
				// alert(JSON.stringify(response));
				photo = response.data;
				updateMainImageTable(response.data);
			}
		});
	}
}

function updateMainImageTable(image) {
	var rows = "";
	if(image)
	{
		rows = "<tr>";
		rows += "<td class='col-md-2' style='vertical-align: middle; text-align: center;'> \
									<img class='img-thumbnail' src='/images/locations/" + image + "'/> \
									<a class='btn btn-lg btn-primary btn-block' style='margin-top: 20px;' onclick=\"tryToDeleteMainImage('" + image +  "')\"> \
									<span class='glyphicon glyphicon-trash' style='color: white;'/>	Delete photo\
								 </a> \
							 </td>";
		rows += "</tr>";
	}
	$('#mainImageTable tbody').html(rows);
}

function tryToDeleteMosqueImage(imageName) {
	var del = confirm("Are you sure?");
	if(del)
	{
		$.ajax({
			type: 'DELETE',
			url: '/v1/locations/' + $('#id').val() + '/mosquelogo',
			error: function(jq, error, message) {
				alert(message);
			},
			success: function(response) {
				// alert(JSON.stringify(response));
				mosqueLogo = response.data;
				updateMosqueImageTable(response.data);
			}
		});
	}
}

function updateMosqueImageTable(image) {
	var rows = "";
	if(image)
	{
		rows = "<tr>";
		rows += "<td class='col-md-2' style='vertical-align: middle; text-align: center;'> \
									<img class='img-thumbnail' src='/images/locations/" + image + "'/> \
									<a class='btn btn-lg btn-primary btn-block' style='margin-top: 20px;' onclick=\"tryToDeleteMosqueImage('" + image +  "')\"> \
									<span class='glyphicon glyphicon-trash' style='color: white;'/>	Delete logo\
								 </a> \
							 </td>";
		rows += "</tr>";
	}
	$('#mosqueImageTable tbody').html(rows);
}

function activateTab() {
	var target = document.getElementById('spinnerMainImage')
	var spinner = new Spinner({scale: 0.5, hwaccel: true, top: '40%'}).spin(target);
	updateMainImageTable(photo);

	// console.log($("#placeType").val());
	if($("#placeType").val() === 'mosque')
	{
		$("#mosqueLogoBlock").removeClass('hidden');
		$("#mosqueLogoBlock").addClass('visible');
		target = document.getElementById('spinnerMosqueImage')
		spinner = new Spinner({scale: 0.5, hwaccel: true, top: '40%'}).spin(target);
		updateMosqueImageTable(mosqueLogo);		
	}
	else
	{
		mosqueLogo = null;
		$("#mosqueLogoBlock").removeClass('visible');
		$("#mosqueLogoBlock").addClass('hidden');
	}
}

$('.nav-tabs a').on('shown.bs.tab', function(event) {
	var activeTab = $(event.target).attr('aria-controls');
	if(activeTab == 'photos')
	{
		activateTab();
	}
});

Dropzone.autoDiscover = false;
