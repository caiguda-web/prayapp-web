var express = require('express');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var config = require('config');
var logger = require('./libs/logger');

// register jobs in agenda
require('./jobs/jobs');

// API routers
var apiUsers = require('./routes/api/users');
var apiPrayers = require('./routes/api/prayers');
var apiNearest = require('./routes/api/nearest');
var apiRooms = require('./routes/api/rooms');
var apiPrayerLocations = require('./routes/api/prayerLocations');
var apiAdmins = require('./routes/api/admins');
var apiSpots = require('./routes/api/spots');
var images = require('./routes/api/images');

var app = express();

app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./middleware/normSend'));

// API
app.use('/v1/users', apiUsers);
app.use('/v1/prayers', apiPrayers);
app.use('/v1/nearest', apiNearest);
app.use('/v1/rooms', apiRooms);
app.use('/v1/locations', apiPrayerLocations);
app.use('/v1/admins', apiAdmins);
app.use('/v1/spots', apiSpots);
app.use('/images', images);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var error = new Error('Not Found');
    error.status = 404;
    next(error);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') != 'development') {
    app.use(function(err, req, res, next) {
			console.log('ERROR HANDLER !!!!!', err);
			var result = {code: err.status || 500, data: null, error: {message: err.message, details: err}};
			res.send(result);
    });
}

var port = config.get("application.apiPort");
app.listen(port);
logger.info("App started on port: %s, environment: %s", port, config.util.getEnv('NODE_ENV'));

module.exports = app;


// var moment = require('moment');
// var dateStr = "2016-03-09'T'20:00:00.+08:00Z";
// var date1 = moment(dateStr, 'YYYY-MM-DDTHH:mm:ss.Z');
// console.log("date1: ", date1);
// var date2 = date1.toDate();
// console.log("date2: ", date2);
// var zone = date2.getTimezoneOffset() / 60;
// console.log("Zone: ", zone);
