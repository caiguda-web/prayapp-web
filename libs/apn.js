var apn = require('apn');
var logger = require("./logger");
var config = require('config');
var path = require('path');

/**
 * use this to prepare certificate: https://github.com/argon/node-apn/wiki/Preparing-Certificates
 * documentation: https://github.com/argon/node-apn/tree/master/doc
 * on npmjs: https://www.npmjs.com/package/apn
**/

var certPath = path.join(__dirname, '../certs/' + config.get("apn.cert"));
var keyPath = path.join(__dirname, '../certs/' + config.get("apn.key"));
// console.log(certPath);
// console.log(keyPath);

var connection = new apn.Connection({
	cert: certPath,
	key: keyPath,
	connectTimeout: 30000
});

connection.on('error', function(error) {
	logger.error("APN connection failed with error: ", error);
});

connection.on('socketError', function(error) {
	logger.error("APN socket connection failed with error: ", error);
});

connection.on('connected', function(openSockets) {
	logger.info("APN connected");	
});

connection.on('transmitted', function(notification, device) {
	logger.info("APN notification %s was transmitted to device %s", JSON.stringify(notification), device.toString());
});

connection.on('transmissionError', function(errorCode, notification, device) {
	logger.info("APN transmissionError: %s for notification %s for device %s", errorCode, JSON.stringify(notification), device ? device.toString() : "null");
});


var feedback = new apn.Feedback({
	cert: certPath,
	key: keyPath,
	connectTimeout: 30000,
	batchFeedback: true,
	interval: 3600 //43200	//twice per day
});

module.exports.Notification = apn.Notification;
module.exports.connection = connection;
module.exports.feedback = feedback;
