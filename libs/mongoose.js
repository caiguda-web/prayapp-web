var mongoose = require("mongoose");
var config = require('config');
var logger = require("./logger");

module.exports = mongoose;
module.exports.ObjectID = require('mongodb').ObjectID;

// mongoose.set('debug', true);

var url = config.get("database.url");
mongoose.connect(url);
var connection = mongoose.connection;

connection.on("error", function(error)
{
	logger.error("Can't connect to MongoDB: " + url + " | Error: " + error);
});

connection.once("open", function()
{
	logger.info("Connected to MongoDB: ", url);
});
