var config = require('config');
var Agenda = require('agenda');
var os = require('os');


var agenda = new Agenda();

var url = config.get("database.url");
agenda.database(url);
agenda.name(os.hostname + '-' + process.pid);
agenda.processEvery("1 minute");

agenda.on('ready', function() {
	agenda.start();	
});

module.exports = agenda;