var winston = require("winston");
var fs = require('fs');
// var Mail = require('winston-mail').Mail;

var logDirectory = __dirname + '/../logs';
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
  
var consoleOptions = {};
consoleOptions.colorize = true;
consoleOptions.prettyPrint = true;

var infoOptions = {};
infoOptions.name = "info-log";
infoOptions.level = "info";
infoOptions.filename = logDirectory + "/info-log";
infoOptions.json = true;


var errorOptions = {};
errorOptions.name = "error-log";
errorOptions.level = "error";
errorOptions.filename = logDirectory + "/error-log";
errorOptions.json = true;
errorOptions.handleExceptions = false;

// var mailOptions = {};
// mailOptions.to = "korshilovskiy@gmail.com";
// mailOptions.from = "";
// mailOptions.username = "";
// mailOptions.password = "";
// mailOptions.level = "info";

var logger = new winston.Logger({
	transports: [
		new winston.transports.Console(consoleOptions),
		new winston.transports.DailyRotateFile(infoOptions),
		new winston.transports.DailyRotateFile(errorOptions)
		// new winston.transports.Mail(mailOptions)
	]
});

module.exports = logger;