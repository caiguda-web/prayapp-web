var util = require('util');
var async = require('async');
var logger = require('./logger');
var passport = require('./passport');
var ObjectID = require('./mongoose').ObjectID;

function paginator(query, sortOptions, page, limit, sessionPageOwner, mainCb) {
	query.count(function(error, count) {
		if(error)
		{
			mainCb(error, null, 0);
		}
		else
		{
			var pagesCount = Math.ceil(count / limit);
			if(page > pagesCount)
				page = pagesCount;
			if(page < 1)
				page = 1;
			sessionPageOwner.page = page;

			var offset = limit * (page - 1);
			query.find();
			query.sort(sortOptions).limit(limit).skip(offset).select('-__v');
			query.exec(function(error, models) {
				mainCb(error, models, pagesCount);
			});
		}
	});
}

function paginatorWithAggregation(modelClass, pipeline, sortOptions, page, limit, sessionPageOwner, mainCb) {
	pipeline.push({$count: 'count'});
	var aggregateCount = modelClass.aggregate(pipeline);
	aggregateCount.exec(function(error, result) {
		if(error)
		{
			return mainCb(error, null, 0);
		}
		
		var pagesCount = (result.length > 0) ? (Math.ceil(result.shift().count / limit)) : 1;
		if(page > pagesCount)
			page = pagesCount;
		if(page < 1)
			page = 1;
		sessionPageOwner.page = page;

		var offset = limit * (page - 1);

		pipeline.pop();	// remove pipeline item $count
		var aggregateModels = modelClass.aggregate(pipeline);
		aggregateModels.append([ 
			{$sort: sortOptions},
			{$skip: offset},
			{$limit: limit}
		]);
		aggregateModels.exec(function(error, models) {
			mainCb(error, models, pagesCount);
		});
	});	
}

module.exports.paginator = paginator;
module.exports.paginatorWithAggregation = paginatorWithAggregation;
