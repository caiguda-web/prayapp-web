var gcm = require('node-gcm');
var logger = require("./logger");
var config = require('config');
var path = require('path');

/**
 * use this to prepare certificate: https://developers.google.com/cloud-messaging/gcm#senderid
 * on npmjs: https://www.npmjs.com/package/node-gcm
**/

var apiKey = config.get("gcm.apiKey");
var sender = new gcm.Sender(apiKey);


module.exports.Message = gcm.Message;
module.exports.sender = sender;
