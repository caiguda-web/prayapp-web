var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Admin = require('../models/admin');
var logger = require("./logger");

//
passport.use('signin-local', new LocalStrategy(
	{
		passReqToCallback: true
	},
	function(req, username, password, done)
	{
		Admin.findOne({username: username}, function(error, admin) {
			if (error)
			{
				console.log("Can't authorize with error:" + error);
				done(error);
			}
			else if (!admin || (admin && !admin.validatePasswordSync(password)))
			{
				done(null, false, req.flash('auth-message', 'Password or Username is invalid'));
			}
			else
			{
				done(null, admin);
			}
		});
	}
));

passport.use('signup-local', new LocalStrategy(
	{
		passReqToCallback: true
	},
	function(req, username, password, done) {
		Admin.createAdmin(username, req.body.password, req.body.email, 'master', function(error, admin) {
			if(error)
			{
				done(null, false, req.flash('auth-message', error.message));
			}
			else
			{
				done(null, admin);
			}
		});
	}
));

var isAuthenticated = function(req, res, next) {
	if (req.isAuthenticated())
		next();
	else
		res.redirect('/signin');
}

var isNotAuthenticated = function(req, res, next) {
	if (!req.isAuthenticated())
		next();
	else
		res.redirect('/home');
}

var isAdminAuthenticated = function(req, res, next) {
	if (req.isAuthenticated() && req.user.role === 'master')
		next();
	else if(req.isAuthenticated())
		res.redirect('/home');
	else
		res.redirect('/signin');
}

module.exports = passport;
module.exports.isAuthenticated = isAuthenticated;
module.exports.isNotAuthenticated = isNotAuthenticated;
module.exports.isAdminAuthenticated = isAdminAuthenticated;
