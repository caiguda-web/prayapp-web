var async = require('async');
var logger = require("../libs/logger");
var Prayer = require('../models/prayer');
var Spot = require("../models/spot");
var VisitedSpot = require("../models/visitedSpot");

function markPartisipantsAsVisitSpot(prayer, mainCb) {
	if(!prayer.spotId)
	{
		return mainCb(null, null);
	}
	
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(prayer.spotId, function(error, spot) {
				cb(error, spot);
			});
		},
		function(spot, cb) {
			if(spot.toDelete)
			{
				return cb(null, spot);
			}
			
			var activeParticipants = prayer.participants.filter(function(participant) {
				return participant.status == 'active' && participant.user.id != spot.owner.id;
			});
			async.each(activeParticipants, function(participant, itCb) {
				VisitedSpot.visitSpot(prayer.spotId, participant.user.id, function(error) {
					if(!error)
					{
						prayer.remindToReviewSpotSync(participant.user.id);				
					}
					itCb(error);
				});		
			},
			function(error) {
				cb(error, spot);
			});
		}
	],
	function(error, spot) {
		if(error && error.status == 404)
			error = null;
		mainCb(error, spot);
	});
}
 
function registerPrayerCloseJob(agenda) {
	agenda.define("PrayerCloseJob", function(job, done) {
		logger.info("PrayerCloseJob: BEGIN");
		var prayerId = job.attrs.data.prayerId;
		
		async.waterfall([
			function(cb) {
				Prayer.findByPrayerId(prayerId, function(error, prayer) {
					cb(error, prayer);
				});
			},
			function(prayer, cb) {
				markPartisipantsAsVisitSpot(prayer, function(error, spot) {
					cb(error, spot);
				});
			},
			function(spot, cb) {
				Prayer.removePrayer(prayerId, function(error) {
					cb(error, spot);
				});
			},
			function(spot, cb) {
				if(spot && spot.toDelete)
				{
					Spot.removeSpot(spot.id, cb);
				}
				else
				{
					cb(null);
				}
			}
		],
		function(error) {
			if(error)
			{
				logger.error("PrayerCloseJob: Job failed:", error);
				job.fail(error);
			}
			logger.info("PrayerCloseJob: END");
			done();
		});		
	});
}

module.exports = registerPrayerCloseJob;