var async = require('async');
var logger = require("../libs/logger");
var Prayer = require('../models/prayer');

function registerPrayerNotificationJob(agenda) {
	agenda.define("PrayerNotificationJob", function(job, done) {
		logger.info("PrayerNotificationJob: BEGIN");
		var prayerId = job.attrs.data.prayerId;
		var participantId = job.attrs.data.participantId;
		async.waterfall([
			function(cb) {
				Prayer.findByPrayerId(prayerId, cb)
			},
			function(prayer, cb) {
				if (prayer.hasParticipantSync(participantId))
				{
					prayer.remindParticipant(participantId, cb);
				}
				else
				{
					cb(null);
				}
			}
		],
		function(error) {
			if(error)
			{
				logger.error("PrayerNotificationJob: Job failed:", error);
				job.fail(error);
			}
			logger.info("PrayerNotificationJob: END");
			done();
		});
	});	
}

module.exports = registerPrayerNotificationJob;