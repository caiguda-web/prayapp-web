var agenda = require('../libs/agenda');
var registerPrayerCloseJob = require('../jobs/prayerCloseJob');
var registerPrayerNotificationJob = require('../jobs/prayerNotificationJob');
var registerPrayerTimesJob = require('../jobs/prayerTimesJob');

registerPrayerCloseJob(agenda);
registerPrayerNotificationJob(agenda);
registerPrayerTimesJob(agenda);