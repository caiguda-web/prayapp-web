var async = require('async');
var logger = require("../libs/logger");
var moment = require('moment');
var PrayerLocation = require('../models/prayerLocation');
var PrayerTimes = require('prayer-times');

function registerPrayerTimesJob(agenda) {
	agenda.define("PrayerTimesJob", function(job, done) {
		logger.info("PrayerTimesJob: BEGIN");
		var prayerLocationId = job.attrs.data.prayerLocationId;
		async.waterfall([
			function(cb) {
				PrayerLocation.findByPrayerLocationId(prayerLocationId, function(error, prayerLocation) {
					cb(error, prayerLocation);
				});
			},
			function(prayerLocation, cb) {
				var prayerTimes = new PrayerTimes();
				prayerTimes.setMethod('ISNA');
				var date = prayerLocation.convertServerDateToLocationDate(new Date());
				// console.log('DATE: ' + date);
				var hourDiff = 60 * prayerLocation.serverToLocationHoursDiff();
				prayerTimes.tune({fajr: hourDiff, dhuhr: hourDiff, asr: hourDiff, maghrib: hourDiff, isha: hourDiff});
				var times = prayerTimes.getTimes(date, [prayerLocation.location.coordinate[1], prayerLocation.location.coordinate[0]], 'auto', 'auto', '24h');
				// console.log(times);
				// console.log('FAJR: ' + times.fajr);
				prayerLocation.timeFajrStart = moment(times.fajr, 'HH:mm').format('hh:mm A');
				prayerLocation.timeDhuhrStart = moment(times.dhuhr, 'HH:mm').format('hh:mm A');
				prayerLocation.timeAsrStart = moment(times.asr, 'HH:mm').format('hh:mm A');
				prayerLocation.timeMaghribStart = moment(times.maghrib, 'HH:mm').format('hh:mm A');
				prayerLocation.timeIshaStart = moment(times.isha, 'HH:mm').format('hh:mm A');
				prayerLocation.save(function(error) {
					cb(error);
				});
			}
		]
		,function(error) {
			if(error) {
				logger.error("PrayerTimesJob: Job failed:", error);
				job.fail(error);
			}
			logger.info("PrayerTimesJob: END");
			done();
		});
	});
}

module.exports = registerPrayerTimesJob;