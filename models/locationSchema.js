var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;

var locationSchema = new Schema({
	coordinate: {
		type: [Number],
		index: '2d'
	},
	address: {
		type: String,
		required: true,
	default: null
	}
},
{
	_id: false
});

module.exports = locationSchema;
