var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var moment = require('moment');
var logger = require("../libs/logger");
// var User = require("./user");
// var Spot = require("./spot");


var reportSchema = new Schema({
	reporter: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
		index: true
	},
	
	reason: {
		type: String,
		maxlength: 1024,
		required: false,
		default: null
	},
	
	created: {
		type: Date,
		required: true
	}
},
{
	_id: false
});

reportSchema.virtual('createdFormatted').get(function() {
	return moment(this.created).format('DD MMM YYYY hh:mm A');
});



var reportedSpotSchema = new Schema({
	
	spot: {
		type: Schema.Types.ObjectId,
		ref: 'Spot',
		required: true,
		index: true
	},

	reports: [reportSchema],
	
	created: {
		type: Date,
		required: true
	}

});


reportedSpotSchema.virtual('createdFormatted').get(function() {
	return moment(this.created).format('DD MMM YYYY hh:mm A');
});

reportedSpotSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.exec(function(error, reportedSpots) {
		mainCb(error, reportedSpots);
	});
}

reportedSpotSchema.statics.findByReportedSpotId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.populate('spot', '-__v');
	query.exec(function(error, reportedSpot) {
		if(!error && !reportedSpot)
		{
			error = new Error("Can't find reportedSpot with such identifier");
			error.status = 404;
		}
		mainCb(error, reportedSpot);
	});	
}

reportedSpotSchema.statics.findFullByReportedSpotId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.populate({
		path: 'spot',
		select: '-__v',
		populate: [
			{
				path: 'owner',
				select: '-__v'
			},
			{
				path: 'reviews.reviewer',
				select: '-__v'
			}
		]
	});
	query.exec(function(error, reportedSpot) {
		if(!error && !reportedSpot)
		{
			error = new Error("Can't find reportedSpot with such identifier");
			error.status = 404;
		}
		mainCb(error, reportedSpot);
	});	
}

reportedSpotSchema.statics.findBySpotId = function(spotId, shouldExists, mainCb) {
	var query = this.findOne({spot: spotId}, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.exec(function(error, reportedSpot) {
		if(!error && !reportedSpot && shouldExists)
		{
			error = new Error("Can't find reportedSpot with such identifier");
			error.status = 404;
		}
		mainCb(error, reportedSpot);
	});	
}

reportedSpotSchema.statics.findByReporterId = function(reporterId, mainCb) {
	var query = this.find({reports: {$elemMatch: {reporter: mongoose.Types.ObjectId(reporterId)}}}, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.exec(function(error, reportedSpots) {
		if(!error && !reportedSpots)
		{
			error = new Error("Can't find reportedSpot(s) with such identifier");
			error.status = 404;
		}
		mainCb(error, reportedSpots);
	});	
}

// function(cb) {
// 	Spot.findBySpotId(spotId, function(error, spot) {
// 		cb(error, spot);
// 	});
// },
// function(spot, cb) {
// 	User.findByUserId(reporterId, function(error, user) {
// 		cb(error, spot, user);
// 	});
// },
reportedSpotSchema.statics.reportSpot = function(spotId, reporterId, reason, mainCb) {
	async.waterfall([
		function(cb) {
			ReportedSpot.findBySpotId(spotId, false, function(error, reportedSpot) {
				cb(error, reportedSpot);
			});
		},
		function(reportedSpot, cb) {
			if(reportedSpot)
			{
				reportedSpot.addReport(reporterId, reason, cb);
			}
			else
			{
				var reportedSpot = new ReportedSpot();
				reportedSpot.spot = spotId;
				reportedSpot.reports = [{
					reporter: reporterId,
					reason: reason,
					created: new Date()
				}];
				reportedSpot.created = new Date();
				reportedSpot.save(function(error, reportedSpot) {
					if(error)
						logger.error("ReportedSpot: Can't save new entity: " + error);
					cb(error, reportedSpot);
				});				
			}
		}
	],
	function(error, reportedSpot) {
		mainCb(error, reportedSpot);
	});
}

reportedSpotSchema.statics.removeReportedSpot = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			ReportedSpot.findByReportedSpotId(identifier, function(error, reportedSpot) {
				cb(error, reportedSpot);
			})
		},
		function(reportedSpot, cb) {
			reportedSpot.remove(function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}

reportedSpotSchema.methods.wasReportedByUser = function(userId) {
	var wasReported = false;
	for(var i = 0; i < this.reports.length; ++i)
	{
		if(this.reports[i].reporter.id === userId)
		{
			wasReported = true;
			break;
		}
	}
	return wasReported;
}

reportedSpotSchema.methods.addReport = function(reporterId, reason, mainCb) {
	if(this.wasReportedByUser(reporterId))
	{
		var error = new Error("User with such identifier has already reported this spot!");
		error.status = 400;
		return mainCb(error, null);
	}
		
	var report = {
					reporter: reporterId,
					reason: reason,
					created: new Date()
	};
	this.reports.push(report);
	this.save(function(error, reportedSpot) {
		if(error)
			logger.error("ReportedSpot: Can't save entity: " + error);
		mainCb(error, reportedSpot);
	});
}

var ReportedSpot = mongoose.model("ReportedSpot", reportedSpotSchema);


module.exports = ReportedSpot;
