var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var moment = require('moment');
var logger = require("../libs/logger");
var agenda = require('../libs/agenda');
var Admin = require("./admin");

var LocationSchema = require("./locationSchema");

var timeValidator = {
	validator: function(v) {
		return v ? /^[0-9]{2}:[0-9]{2} [AP]M$/.test(v) : true;
	},
	message: "{VALUE} isn't valid time format!"
}

var prayerLocationSchema = new Schema({
	
	name: {
		type: String,
		maxlength: 128,
		required: true
	},

	location: LocationSchema,
	
	placeType: {
		type: String,
		required: true,
		enum: ['mosque', 'room', 'chapel', 'other']
	},

	photo: {
		type: String,
		required: false,
		default: null
	},

	mosqueLogo: {
		type: String,
		required: false,
		default: null
	},
	
	timeOpen: {
		type: String,
		required: false,
		default: null,
		validate: timeValidator
	},
	
	timeClose: {
		type: String,
		required: false,
		default: null,
		validate: timeValidator
	},
	
	alwaysOpen: {
		type: Boolean,
		required: false,
		default: false
	},

	timeFajrStart: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeFajrIqamah: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeDhuhrStart: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeDhuhrIqamah: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeAsrStart: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeAsrIqamah: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeMaghribStart: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeMaghribIqamah: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeIshaStart: {
		type: String,
		required: false,
		validate: timeValidator
	},

	timeIshaIqamah: {
		type: String,
		required: false,
		validate: timeValidator
	},

	managerUsername: {
		type: String,
		required: true,
		index: true
	}	
},
{
	toJSON: {
		virtuals: true
	},
	collation: {
		locale: 'en_US',
		strength: 3
	}
});

prayerLocationSchema.pre('save', function(next) {
	if(this.placeType != 'mosque')
	{
		PrayerLocation.removeFile(this.mosqueLogo);		
	}
	next(null);
});

prayerLocationSchema.pre('remove', function(next) {
	PrayerLocation.removeFile(this.photo);
	PrayerLocation.removeFile(this.mosqueLogo);
	agenda.cancel({name: 'PrayerTimesJob', 'data.prayerLocationId': this.id}, function(error, numRemoved) {
		next(error);		
	});
});

// prayerLocationSchema.post('save', function(doc, next) {
	// var serverHour = doc.convertLocationTimeToServerTime(2);
	// var timeStr = serverHour + ':00';
	// // console.log('timeStr: ' + timeStr);
	// var job = agenda.create('PrayerTimesJob', {prayerLocationId: doc.id});
	// job.unique({'data.prayerLocationId': doc.id});
	// job.schedule('today ' + timeStr);
	// job.repeatAt('tomorrow ' + timeStr);	// see https://github.com/rschmukler/agenda/issues/99
	// job.save(function(error) {
	// 	if(error)
	// 	{
	// 		logger.error("Can't schedule job for PrayerLocation: " + error);
	// 	}
	// 	else
	// 	{
	// 		logger.info("Schedule PrayerTimesJob: " + timeStr);
	// 	}
	// 	next();
	// });
// });

prayerLocationSchema.virtual('manager', {
	ref: 'Admin',
	localField: 'managerUsername',
	foreignField: 'username'
});

prayerLocationSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v');
	query.exec(function(error, prayerLocations) {
		mainCb(error, prayerLocations);
	});
}

prayerLocationSchema.statics.findByPrayerLocationId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate('manager', "id username");
	query.exec(function(error, prayerLocation) {
		if(!error && !prayerLocation)
		{
			error = new Error("Can't find prayerLocation with such identifier");
			error.status = 404;
		}
		mainCb(error, prayerLocation);		
	});	
}

prayerLocationSchema.statics.findByManagerUsername = function(managerUsername, mainCb) {
	var query = this.find({managerUsername: managerUsername}, '-__v');
	query.populate('manager', "id username");
	query.exec(function(error, prayerLocations) {
		// if(!error && !prayerLocations)
		// {
		// 	error = new Error("Can't find prayerLocation with such identifier");
		// 	error.status = 404;
		// }
		mainCb(error, prayerLocations);
	});	
}

prayerLocationSchema.statics.findByIdAndManagerUsername = function(identifier, managerUsername, mainCb) {
	var query = this.findOne({_id: identifier, managerUsername: managerUsername}, '-__v');
	query.populate('manager', "id username");
	query.exec(function(error, prayerLocation) {
		if(!error && !prayerLocation)
		{
			error = new Error("Can't find prayerLocation with such identifier and manager's username");
			error.status = 404;
		}
		mainCb(error, prayerLocation);		
	});	
}

prayerLocationSchema.statics.nearestLocations = function(latitude, longitude, radius, mainCb) {
	var degrees = radius / 110574.0; // Latitude: 1 deg = 110.574 km
	var area = {center:[longitude, latitude], radius: degrees};
	var query = PrayerLocation.find();
	query.where('location.coordinate').circle(area).select('-__v');	
	query.exec(function(error, prayerLocations) {
		mainCb(error, prayerLocations);
	});
}

prayerLocationSchema.statics.createPrayerLocation = function(
	name,
	latitude, longitude, address,
	placeType,
	timeOpen,
	timeClose,
	alwaysOpen,
	timeFajrStart,
	timeFajrIqamah,
	timeDhuhrStart,
	timeDhuhrIqamah,
	timeAsrStart,
	timeAsrIqamah,
	timeMaghribStart,
	timeMaghribIqamah,
	timeIshaStart,
	timeIshaIqamah,
	managerUsername,
	mainCb
) {
	async.waterfall([
		function(cb) {
			Admin.findByUsername(managerUsername, true, cb);
		},
		function(manager, cb){
			var prayerLocation = new PrayerLocation();
			prayerLocation.name = name;
			prayerLocation.location = {
				coordinate:[longitude, latitude],
				address: address ? address : null
			};
			prayerLocation.placeType = placeType;
			prayerLocation.photo = null;
			if(alwaysOpen === true)
			{
				prayerLocation.alwaysOpen = alwaysOpen;
			}
			else
			{
				prayerLocation.timeOpen = timeOpen;
				prayerLocation.timeClose = timeClose;				
			}			
			prayerLocation.timeFajrStart = timeFajrStart
			prayerLocation.timeFajrIqamah = timeFajrIqamah;
			prayerLocation.timeDhuhrStart = timeDhuhrStart;
			prayerLocation.timeDhuhrIqamah = timeDhuhrIqamah;
			prayerLocation.timeAsrStart = timeAsrStart;
			prayerLocation.timeAsrIqamah = timeAsrIqamah;
			prayerLocation.timeMaghribStart = timeMaghribStart;
			prayerLocation.timeMaghribIqamah = timeMaghribIqamah;
			prayerLocation.timeIshaStart = timeIshaStart;
			prayerLocation.timeIshaIqamah = timeIshaIqamah;
			prayerLocation.managerUsername = managerUsername;
			prayerLocation.save(function(error, prayerLocation) {
				if(error)
					logger.error("PrayerLocation: Can't save new entity: " + error);
				else
					prayerLocation.scheduleUpdatePrayerTimes();

				cb(error, prayerLocation);
			});
		}
	],
	function(error, prayerLocation) {
		mainCb(error, prayerLocation);
	});
}

prayerLocationSchema.statics.removePrayerLocation = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(identifier, function(error, prayerLocation) {
				cb(error, prayerLocation);
			})
		},
		function(prayerLocation, cb) {
			prayerLocation.remove(function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}

prayerLocationSchema.statics.removeFile = function(fileName) {
	if(fileName && fileName.length > 0)
	{
		var filePath = path.join(__dirname, '../public/images/locations/' + fileName);
		fs.unlink(filePath, function(error) {
			if(error)
				logger.error("Can't remove old file:" + filePath + " | May be unexist. Error:" + error);
		});
	}
}

prayerLocationSchema.methods.updatePhotoFile = function(photo, mainCb) {
	if(!photo)
	{
		var error = new Error("File object isn't provided");
		error.status = 400;
		return mainCb(error);
	}
	
	var self = this;
	async.waterfall([
		function(cb) {
			var oldPath = photo.path;
			var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
			var newPath = path.join(__dirname, '../public/images/locations/' + fileName);
			fs.move(oldPath, newPath, function(error) {
				cb(error, fileName);
			});
		},
		function(fileName, cb) {
			self.updatePhoto(fileName, cb);
		},
	],
	function(error, prayerLocation) {
		mainCb(error, error ? null : prayerLocation);
	});
}

prayerLocationSchema.methods.updatePhoto = function(fileName, mainCb) {
	var oldPhoto = this.photo;
	this.photo = fileName;
	this.save(function(error, prayerLocation) {
		if(error)
		{
			PrayerLocation.removeFile(fileName);
			logger.error("PrayerLocation: Can't save new entity: " + error);
			mainCb(error, null);
		}
		else
		{
			PrayerLocation.removeFile(oldPhoto);
			mainCb(null, prayerLocation);
		}
	});
}

prayerLocationSchema.methods.updateMosqueLogoFile = function(photo, mainCb) {
	if(!photo)
	{
		var error = new Error("File object isn't provided");
		error.status = 400;
		return mainCb(error);
	}
	
	var self = this;
	async.waterfall([
		function(cb) {
			var oldPath = photo.path;
			var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
			var newPath = path.join(__dirname, '../public/images/locations/' + fileName);
			fs.move(oldPath, newPath, function(error) {
				cb(error, fileName);
			});
		},
		function(fileName, cb) {
			self.updateMosqueLogo(fileName, cb);
		},
	],
	function(error, prayerLocation) {
		mainCb(error, error ? null : prayerLocation);
	});
}

prayerLocationSchema.methods.updateMosqueLogo = function(fileName, mainCb) {
	var oldPhoto = this.mosqueLogo;
	this.mosqueLogo = fileName;
	this.save(function(error, prayerLocation) {
		if(error)
		{
			PrayerLocation.removeFile(fileName);
			logger.error("PrayerLocation: Can't save new entity: " + error);
			mainCb(error, null);
		}
		else
		{
			PrayerLocation.removeFile(oldPhoto);
			mainCb(null, prayerLocation);
		}
	});
}

prayerLocationSchema.methods.updateLocation = function(latitude, longitude, address, mainCb) {
	this.location = {
		coordinate: [longitude, latitude],
		address: address
	};
	this.save(function(error, prayerLocation) {
		if(error)
			logger.error("PrayerLocation: Can't save entity: " + error);
		else
			prayerLocation.scheduleUpdatePrayerTimes();
		mainCb(error, prayerLocation);
	});
}

prayerLocationSchema.methods.scheduleUpdatePrayerTimes = function(mainCb) {
	var serverHour = this.convertLocationTimeToServerTime(2);
	var timeStr = serverHour + ':00';
	// console.log('timeStr: ' + timeStr);
	var job = agenda.create('PrayerTimesJob', {prayerLocationId: this.id});
	job.unique({'data.prayerLocationId': this.id});
	job.schedule('today ' + timeStr);
	job.repeatAt('tomorrow ' + timeStr);	// see https://github.com/rschmukler/agenda/issues/99
	job.save(function(error) {
		if(error)
			logger.error("Can't schedule job for PrayerLocation: " + error);
		else
			logger.info("Schedule PrayerTimesJob: " + timeStr);

		if(mainCb)
			mainCb(error);
	});
}

prayerLocationSchema.methods.convertLocationTimeToServerTime = function(hour) {
	var rawLocationOffset = Math.floor(this.location.coordinate[0] * 24 / 360);
	var serverOffset = -Math.floor((new Date()).getTimezoneOffset() / 60);	
	var serverHour = (hour - rawLocationOffset) + serverOffset;

	if(serverHour < 0)
		serverHour += 24;
	else if(serverHour >= 24)
		serverHour -= 24;

	return serverHour;
}

prayerLocationSchema.methods.convertServerDateToLocationDate = function(serverDate) {
	var date = new Date(serverDate.getTime());
	var rawLocationOffset = Math.floor(this.location.coordinate[0] * 24 / 360);
	var serverOffset = -Math.floor(date.getTimezoneOffset() / 60);	
	date = moment(date).add(- serverOffset + rawLocationOffset, 'h').toDate();	
	return date;
}

prayerLocationSchema.methods.serverToLocationHoursDiff = function() {
	var rawLocationOffset = Math.floor(this.location.coordinate[0] * 24 / 360);
	var serverOffset = -Math.floor((new Date()).getTimezoneOffset() / 60);	
	var hourDiff = - serverOffset + rawLocationOffset;
	return hourDiff;
}

var PrayerLocation = mongoose.model("PrayerLocation", prayerLocationSchema);


module.exports = PrayerLocation;
