var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var moment = require('moment');
var logger = require("../libs/logger");
// var User = require("./user");
// var Spot = require("./spot");


var visitedSpotSchema = new Schema({
	
	spot: {
		type: Schema.Types.ObjectId,
		ref: 'Spot',
		required: true,
		index: true
	},

	visitor: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
		index: true
	},
	
	lastVisit: {
		type: Date,
		required: true
	}

});


visitedSpotSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("reports.reporter", "_id name surname avatar sect gender");
	query.exec(function(error, visitedSpots) {
		mainCb(error, visitedSpots);
	});
}

visitedSpotSchema.statics.findByVisitedSpotId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("visitor", "_id name surname avatar sect gender");
	query.exec(function(error, visitedSpot) {
		if(!error && !visitedSpot)
		{
			error = new Error("Can't find visitedSpot with such identifier");
			error.status = 404;
		}
		mainCb(error, visitedSpot);
	});	
}

visitedSpotSchema.statics.findBySpotId = function(spotId, mainCb) {
	var query = this.find({spot: spotId}, '-__v');
	query.populate("spot", "-__v -reviews");
	query.populate("visitor", "_id name surname avatar sect gender");
	query.exec(function(error, visitedSpots) {
		mainCb(error, visitedSpots);
	});	
}

visitedSpotSchema.statics.findByVisitorId = function(visitorId, mainCb) {
	var query = this.find({visitor: visitorId}, '-__v');
	// query.populate("spot", "-__v -reviews");
	// query.populate("visitor", "_id name surname avatar sect gender");
	query.exec(function(error, visitedSpots) {
		mainCb(error, visitedSpots);
	});	
}

visitedSpotSchema.statics.findBySpotAndVisitor = function(spotId, visitorId, mainCb) {
	var query = this.findOne({spot: spotId, visitor: visitorId}, '-__v');
	// query.populate("spot", "-__v -reviews");
	// query.populate("visitor", "_id name surname avatar sect gender");
	query.exec(function(error, visitedSpot) {
		mainCb(error, visitedSpot);
	});	
}

// function(cb) {
// 	Spot.findBySpotId(spotId, function(error, spot) {
// 		cb(error, spot);
// 	});
// },
// function(spot, cb) {
// 	User.findByUserId(visitorId, function(error, user) {
// 		cb(error, spot, user);
// 	});
// },
// spot and user should exist!
visitedSpotSchema.statics.visitSpot = function(spotId, visitorId, mainCb) {
	async.waterfall([
		function(cb) {
			VisitedSpot.findBySpotAndVisitor(spotId, visitorId, function(error, visitedSpot) {
				cb(error, visitedSpot);
			});
		},
		function(visitedSpot, cb) {
			if(visitedSpot)
			{
				visitedSpot.lastVisit = new Date();
			}
			else
			{
				visitedSpot = new VisitedSpot();
				visitedSpot.spot = spotId;
				visitedSpot.visitor = visitorId;
				visitedSpot.lastVisit = new Date();
			}
			visitedSpot.save(function(error, visitedSpot) {
				if(error)
					logger.error("VisitedSpot: Can't save new entity: " + error);
				cb(error, visitedSpot);
			});				
		}
	],
	function(error, visitedSpot) {
		mainCb(error, error ? null : visitedSpot);
	});
}

visitedSpotSchema.statics.removeVisitedSpot = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			VisitedSpot.findByVisitedSpotId(identifier, function(error, visitedSpot) {
				cb(error, visitedSpot);
			})
		},
		function(visitedSpot, cb) {
			visitedSpot.remove(function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}


var VisitedSpot = mongoose.model("VisitedSpot", visitedSpotSchema);


module.exports = VisitedSpot;
