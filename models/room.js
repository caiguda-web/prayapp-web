var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var logger = require("../libs/logger");
var apn = require('../libs/apn');
var User = require("./user");

var roomParticipantSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true		
	}
},
{
	_id: false
});

var messageSchema = new Schema({
	
	text: {
		type: String,
		required: false,
		default: null
	},
	
	senderId: {
		type: Schema.Types.ObjectId,
		required: true
	},
	
	date: {
		type: Date,
		required: true
	}
	
},
{
	_id: false
});

var roomSchema = new Schema({
	
	prayerId: {
		type: Schema.Types.ObjectId,
		required: true
	},
	
	participants: [roomParticipantSchema],
	
	messages: [messageSchema],
	
	lastMessage: messageSchema
	
});

roomSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v -messages');
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, rooms) {
		mainCb(error, rooms);
	});
}

roomSchema.statics.findByRoomId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender pushTokens messagesNotificationEnabled");
	query.exec(function(error, room) {
		if(!error && !room)
		{
			error = new Error("Can't find room with such identifier");
			error.status = 404;
		}
		mainCb(error, room);
	});
}

roomSchema.statics.findByPrayerId = function(prayerId, mainCb) {
	var query = this.find({prayerId: prayerId}, '-__v -messages');
	query.sort({"lastMessage.date": -1});
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, rooms) {
		mainCb(error, rooms);
	});
}

roomSchema.statics.findByPrayerAndParticipant = function(prayerId, participantId, mainCb) {
	var query = this.find({prayerId: prayerId, participants: {$elemMatch: {user: mongoose.Types.ObjectId(participantId)}}}, '-__v -messages');
	query.sort({"lastMessage.date": -1});
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, rooms) {
		mainCb(error, rooms);
	});
}

roomSchema.statics.findByParticipantId = function(participantId, mainCb) {
	var query = this.find({participants: {$elemMatch: {user: mongoose.Types.ObjectId(participantId)}}}, '-__v -messages');
	query.sort({"lastMessage.date": -1});
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, rooms) {
		mainCb(error, rooms);
	});
}

roomSchema.statics.roomInfoByRoomId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v -messages');
	// query.populate("prayer", "-participants -photo");
	query.populate("participants.user", "_id name surname avatar sect signedIn gender pushTokens messagesNotificationEnabled");
	query.exec(function(error, room) {
		if(!error && !room)
		{
			error = new Error("Can't find room with such identifier");
			error.status = 404;
		}
		mainCb(error, room);
	});
}

roomSchema.statics.messagesByRoomId = function(roomId, mainCb) {
	var query = this.findById(roomId, '-__v');
	query.exec(function(error, room) {
		if(!error && !room)
		{
			error = new Error("Can't find room with such identifier");
			error.status = 404;
		}
		mainCb(error, room ? room.messages : null);
	});
}

roomSchema.statics.createRoom = function(prayerId, participantIds, mainCb)
{
	// console.log("participantIds: ", participantIds);
	var self = this;
	async.map(participantIds, function(id, iterationCb) {
		async.waterfall([
			function(cb) {
				User.findByUserId(id, cb);
			},
			function(user, cb)
			{
				if(user)
				{
					var participant = {};
					participant.user = user;
					cb(null, participant);
				}
				else
				{
					var error = new Error('One of users is not exist');
					error.status = 404;
					cb(error, null);
				}
			}
		],
		function(error, participant) {
			iterationCb(error, participant);
		});
	},
	function(error, participants) {
		if(error)
			return mainCb(error, null);

		async.waterfall([
			function(cb) {
				var room = new Room();
				room.prayerId = prayerId;
				room.participants = participants;
				room.save(function(error, room) {
					if(error)
						logger.error("Room: Can't save new entity: " + error);
					cb(error, room.id);
				});
			},
			function(roomId, cb) {
				Room.findByRoomId(roomId, cb);
			}
		],
		function(error, room) {
			mainCb(error, room);
		});
	});
}

roomSchema.methods.sendMessage = function(senderId, date, text, mainCb) {
	var sender = this.participantByUserIdSync(senderId);
	if(!sender)
	{
		var error = new Error("Room doesn't contain such participant");
		error.status = 400;
		return mainCb(error, null);
	}

	var self = this;
	async.waterfall([
		function(cb) {
			var message = {
				text: text,
				senderId: senderId,
				date: date
			};
			self.lastMessage = message;
			self.messages.push(message);
			self.save(function(error, room) {
				if(error)
					logger.error("Room: Can't save new entity: " + error);
				cb(error, room.id);
			});
		},
		function(roomId, cb) {
			Room.findByRoomId(roomId, cb);
		},
		function(room, cb) {
			var receivers = [];
			for(var i = 0; i < room.participants.length; ++i)
			{
				if(room.participants[i].user.id != senderId)
				{
					receivers.push(room.participants[i]);
				}
			}
			receivers.forEach(function(receiver) {
				// console.log("Message Notification: ", receiver.user);
				if(receiver.user.messagesNotificationEnabled)
				{
					var textLength = text.length < 140 ? text.length : 140;
					var message = text.substring(0, textLength);
					var notification = {};
					notification.alert =  sender.user.fullName + ': ' + message;
					notification.sound = "default";
					notification.payload = {
						type: "message",
						roomId: room.id,
						prayerId: room.prayerId,
						senderId: senderId,
						receiverId: receiver.user.id,
						date: date,
						message: message
					};
					notification.clickAction = "message";
					// console.log("receiver.user: ", receiver.user);
					receiver.user.pushNotification(notification);
				}
			});
			cb(null, room);
		}
	],
	function(error, room) {
		mainCb(error, room);
	});
}

roomSchema.statics.removeRooms = function(prayerId, mainCb)
{
	async.waterfall([
		function(cb) {
			Room.findByPrayerId(prayerId, function(error, rooms) {
				cb(error, rooms);
			});
		},
		function(rooms, cb) {
			async.each(rooms, function(room, itCb) {
				room.remove(function(error) {
					itCb(error);
				});
			},
			function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}

roomSchema.methods.participantByUserIdSync = function(userId) {
	var result = null;
	for(var i = 0; i < this.participants.length; ++i)
	{
		if(this.participants[i].user.id == userId)
		{
			result = this.participants[i];
			break;
		}
	}
	return result;
}

roomSchema.methods.hasParticipantSync = function(userId) {
	return this.participantByUserIdSync(userId) != null;
}

var Room = mongoose.model("Room", roomSchema);

module.exports = Room;
