var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var moment = require('moment');
var apn = require('../libs/apn');
var logger = require("../libs/logger");
var agenda = require('../libs/agenda');
var User = require("./user");
var Room = require("./room");
var LocationSchema = require("./locationSchema");

var participantSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	status: {
		type: String,
		enum: ['unknown', 'invited', 'active'],
	default: 'unknown'
	}
},
{
	_id: false
});

var prayerSchema = new Schema({
	
	prayerType: {
		type: String,
		required: true,
		enum: ['fajr', 'dhuhr', 'asr', 'maghrib', 'isha']
	},
	
	date: {
		type: Date,
		required: true
	},
	
	closeDate: {
		type: Date,
		required: true
	},

	sect: {
		type: String,
		required: true,
		enum: ['sunni', 'shia', 'sufi', 'nopref']
	},
	
	photo: {
		type: String,
		required: false,
	default: null
	},
	
	location: LocationSchema,
	
	owner: {
		type: participantSchema,
		required: true
	},
	
	participants: [participantSchema],
	
	spotId: {
		type: Schema.Types.ObjectId,
		required: false,
		default: null
	}
	
});

prayerSchema.index({prayerType: 1, sect: 1, spotId: 1});
prayerSchema.index({'location.coordinate': '2d', prayerType: 1, sect: 1});

prayerSchema.pre('remove', function(next) {
	Prayer.removeFile(this.photo);
	var self = this;
	async.waterfall([
		function(cb) {
			agenda.cancel({name: 'PrayerNotificationJob', 'data.prayerId': self.id}, function(error, numRemoved) {
				cb(error);
			});
		},
		function(cb) {
			agenda.cancel({name: 'PrayerCloseJob', 'data.prayerId': self.id}, function(error, numRemoved) {
				cb(error);
			});
		},
		function(cb) {
			Room.removeRooms(self.id, cb);
		}
	],
	function(error) {
		next(error);		
	});
});

prayerSchema.virtual('prayerName').get(function() {
	return this.prayerType.charAt(0).toUpperCase() + this.prayerType.slice(1);
});

prayerSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v');
	query.populate("participants.user", "_id name surname avatar sect signedIn gender");
	query.populate("owner.user", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, prayers) {
		mainCb(error, prayers);
	});
}

prayerSchema.statics.findByPrayerId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate("participants.user", "_id name surname avatar sect pushTokens signedIn gender prayReminder messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect pushTokens signedIn gender prayReminder messagesNotificationEnabled");
	query.exec(function(error, prayer) {
		if(!error && !prayer)
		{
			error = new Error("Can't find prayer with such identifier");
			error.status = 404;
		}
		mainCb(error, prayer);		
	});	
}

prayerSchema.statics.infoByPrayerId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v -location');
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, prayer) {
		if(!error && !prayer)
		{
			error = new Error("Can't find prayer with such identifier");
			error.status = 404;
		}
		mainCb(error, prayer);		
	});	
}

prayerSchema.statics.findPrayers = function(prayerType, sect, spotId, mainCb) {
	var query = this.find();
	query.select('-__v');
	if(prayerType)
		query.where('prayerType').equals(prayerType);
	if(sect && sect != 'nopref')
		query.where('sect').equals(sect);
	if(spotId)
		query.where('spotId').equals(spotId);
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, prayers) {
		mainCb(error, prayers);
	});
}

prayerSchema.statics.findByOwnerId = function(ownerId, mainCb) {
	var query = this.findOne({'owner.user': ownerId}, '-__v');
	query.populate("participants.user", "_id name surname avatar sect pushTokens signedIn gender messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect pushTokens signedIn gender messagesNotificationEnabled");
	query.exec(function(error, prayer) {
		if(!error && !prayer)
		{
			error = new Error("Can't find prayer for such owner");
			error.status = 404;
		}
		mainCb(error, prayer);		
	});	
}

prayerSchema.statics.removePrayer = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(identifier, function(error, prayer) {
				cb(error, prayer);
			})
		},
		function(prayer, cb) {
			prayer.remove(function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}

prayerSchema.statics.nearestPrayers = function(prayerType, sect, latitude, longitude, radius, mainCb) {	
	var degrees = radius / 110574.0; // Latitude: 1 deg = 110.574 km
	var area = {center:[longitude, latitude], radius: degrees};
	var query = Prayer.find();
	query.where('location.coordinate').circle(area).select('-__v');
	
	// var query = sect == 'nopref' ? Prayer.find() : Prayer.find({$or: [{'sect': sect}, {'sect': 'nopref'}]})
	if(prayerType && prayerType != 'unknown')
		query.where('prayerType').equals(prayerType);
	if(sect && sect != 'nopref')
		query.or([{'sect': sect}, {'sect': 'nopref'}]);
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, prayers) {
		mainCb(error, prayers);
	});
}

prayerSchema.statics.findByParticipantId = function(participantId, participantStatus, mainCb) {	
	var query;
	if(participantStatus == 'active' || participantStatus == 'invited')
		query = this.find({participants: {$elemMatch: {user: mongoose.Types.ObjectId(participantId), status: participantStatus}}}, '-__v');
	else
		query = this.find({participants: {$elemMatch: {user: mongoose.Types.ObjectId(participantId)}}}, '-__v');
	query.populate("participants.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.populate("owner.user", "_id name surname avatar sect signedIn gender messagesNotificationEnabled");
	query.exec(function(error, prayers) {
		mainCb(error, prayers);
	});	
}

prayerSchema.statics.invitationsCount = function(participantId, mainCb) {	
	var query = this.find({participants: {$elemMatch: {user: mongoose.Types.ObjectId(participantId), status: 'invited'}}}).count();
	query.exec(function(error, count) {
		mainCb(error, count);
	});	
}

prayerSchema.methods.updatePhoto = function(photo, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			var oldPath = photo.path;
			var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
			var newPath = path.join(__dirname, '../public/images/prayers/' + fileName);
			// console.log("newPath: ", newPath);
			fs.move(oldPath, newPath, function(error) {
				cb(error, fileName);
			});
		},
		function(fileName, cb) {
			var oldPhoto = self.photo;
			self.photo = fileName;
			self.save(function(error, prayer) {
				if(error)
				{
					Prayer.removeFile(fileName);
					logger.error("Prayer: Can't save new entity: " + error);
				}
				else
				{
					Prayer.removeFile(oldPhoto);
				}
				cb(error, prayer);
			});
		},
	],
	function(error, prayer) {
		mainCb(error, error ? null : prayer);
	});
}

prayerSchema.methods.updateLocation = function(latitude, longitude, address, mainCb) {
	this.location = {
		coordinate: [longitude, latitude],
		address: address
	};
	this.save(function(error, prayer) {
		if(error)
			logger.error("Prayer: Can't save entity: " + error);
		mainCb(error, prayer);
	});
}

prayerSchema.statics.removeFile = function(fileName) {
	if(fileName && fileName.length > 0)
	{
		var filePath = path.join(__dirname, '../public/images/prayers/' + fileName);
		fs.unlink(filePath, function(error) {
			if(error)
				logger.error("Can't remove old file:" + filePath + " | May be unexist. Error:" + error);
		});
	}
}

prayerSchema.statics.createPrayer = function(ownerId, prayerType, date, closeDate, sect, participantIds, latitude, longitude, address, spotId, mainCb)
{
	// var closeDate = moment(date);
	// var now = new Date();
	// closeDate.add(5, 'minutes');
	// closeDate = closeDate.toDate();

	// console.log("participantIds: ", participantIds);
	var self = this;
	async.map(participantIds, function(id, iterationCb) {
		async.waterfall([
			function(cb) {
				User.findByUserId(id, cb);
			},
			function(user, cb)
			{
				if(user)
				{
					var participant = {};
					participant.user = user;
					cb(null, participant);
				}
				else
				{
					var error = new Error("One of users doesn't exist");
					error.status = 404;
					cb(error, null);
				}
			}
		],
		function(error, participant) {
			iterationCb(error, participant);
		});
	}, 
	function(error, participants) {
		if(error)
			return mainCb(error, null);

		async.waterfall([
			function(cb) {
				User.findByUserId(ownerId, cb);
			},
			function(owner, cb) {
				var prayer = new Prayer();
				prayer.prayerType = prayerType;
				prayer.date = date;
				prayer.closeDate = closeDate;
				prayer.sect = sect;
				prayer.owner = {user: owner, status: 'active'};
				participants.splice(0,0, prayer.owner);
				prayer.participants = participants;
				prayer.spotId = spotId;
				// console.log("participants: ", participants);
				prayer.location = {
					coordinate:[longitude, latitude],
					address: address ? address : null
				};
				prayer.save(function(error, prayer) {
					if(error)
						logger.error("Prayer: Can't save new entity: " + error);
					cb(error, error ? null : prayer);
				});
			},
			function(prayer, cb) {
				prayer.populate("participants.user", "_id name surname avatar sect pushTokens signedIn gender prayReminder messagesNotificationEnabled")
							.populate("owner.user", "_id name surname avatar sect pushTokens signedIn gender prayReminder messagesNotificationEnabled",
							function(error, prayer) {
								cb(error, prayer);
							});
				// Prayer.findByPrayerId(prayerId, cb);
			},
			function(prayer, cb) {
				prayer.scheduleCloseJob();
				async.each(prayer.participants, function(participant, itCb) {
					prayer.inviteParticipant(participant.user.id, function(error){
						itCb(error);
					});					
				}, function(error) {
					cb(error, prayer);
				});
			},
			function(prayer, cb) {
				prayer.scheduleNotificationJob(prayer.owner.user.id, function(error) {
					cb(error, prayer);
				});
			}
		],
		function(error, prayer) {
			mainCb(error, prayer);
		});
	});
}

prayerSchema.methods.userAcceptInvitation = function(userId, mainCb) {
	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error, null);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status != 'invited')
	{
		var error = new Error("User that wasn't invited, can't accept invitation");
		error.status = 400;
		return mainCb(error, null);
	}	

	var self = this;
	async.waterfall([
		function(cb) {
			participant.status = 'active';
			self.save(function(error, prayer) {
				if(error)
					logger.error("Prayer: Can't save new entity: " + error);
				cb(error, participant);
			});
		},
		function(participant, cb) {
			self.scheduleNotificationJob(participant.user.id, cb);
		}
	],
	function(error) {
		mainCb(error, self);		
	});
}

prayerSchema.methods.userJoin = function(userId, mainCb) {
	var participant = this.participantByUserIdSync(userId);
	if(participant && participant.status == 'active')
	{
		var error = new Error("User already active participant in this prayer");
		error.status = 400;
		return mainCb(error, null);
	}	

	var self = this;
	async.waterfall([
		function(cb) {
			User.findByUserId(userId, cb);
		},
		function(user, cb) {
			if(!participant)
			{
				participant = {};
				participant.user = user;
				participant.status = 'active';
				self.participants.push(participant);
			}
			else if(participant.status == 'invited')
			{
				participant.status == 'active';
			}
			self.save(function(error, prayer) {
				if(error)
					logger.error("Prayer: Can't save new entity: " + error);
				cb(error, participant);
			});
		},
		function(participant, cb) {
			self.scheduleNotificationJob(participant.user.id, cb);
		}
	],
	function(error) {
		mainCb(error, self);		
	});
}

prayerSchema.methods.userDeclineInvitation = function(userId, mainCb) {
	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error, null);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status != 'invited')
	{
		var error = new Error("User that wasn't invited, can't decline invitation");
		error.status = 400;
		return mainCb(error, null);
	}	
	
	this.removeParticipantSync(userId);

	this.save(function(error, prayer) {
		if(error)
			logger.error("Prayer: Can't save new entity: " + error);
		mainCb(error, prayer);
	});
}

prayerSchema.methods.userLeave = function(userId, mainCb) {
	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error, null);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status != 'active')
	{
		var error = new Error("User that wasn't active, can't leave prayer");
		error.status = 400;
		return mainCb(error, null);
	}	

	var self = this;
	async.waterfall([
		function(cb) {
			self.removeParticipantSync(userId);
			self.save(function(error, prayer) {
				if(error)
					logger.error("Prayer: Can't save new entity: " + error);
				cb(error, prayer);
			});
		},
		function(prayer, cb) {
			prayer.cancelNotificationJob(participant.user.id, function(error) {
				cb(error, prayer);
			});
		}
	],
	function(error, prayer)
	{
		mainCb(error, prayer);
	});
}

prayerSchema.methods.participantByUserIdSync = function(userId) {
	var result = null;
	for(var i = 0; i < this.participants.length; ++i)
	{
		if(this.participants[i].user.id === userId)
		{
			result = this.participants[i];
			break;
		}
	}
	return result;
}

prayerSchema.methods.hasParticipantSync = function(userId) {
	return this.participantByUserIdSync(userId) != null;
}

prayerSchema.methods.removeParticipantSync = function(userId) {
	for(var i = 0; i < this.participants.length; ++i)
	{
		if(this.participants[i].user.id === userId)
		{
			this.participants.splice(i, 1);
			break
		}
	}
}

prayerSchema.methods.inviteParticipant = function(userId, mainCb) {
	if(this.owner.user.id === userId)
	{
		return mainCb(null);
	}

	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status != 'unknown')
	{
		var error = new Error("Can't invite user, that was already invited");
		error.status = 400;
		return mainCb(error);
	}
	
	var self = this;
	async.waterfall([
		function(cb) {
			participant.status = 'invited';
			self.save(function(error, prayer) {
				if(error)
					logger.error("Prayer: Can't save new entity: " + error);
				cb(error);
			});
		},
		function(cb) {
			Prayer.invitationsCount(participant.user.id, cb);
		},
		function(invitationsCount, cb) {
			// TODO: push notification to invite user
			// console.log("Will create notification");
			
			var notification = {};
			notification.alert =  self.owner.user.fullName + ' has invited you to join the ' + self.prayerName + ' prayer';
			notification.sound = "default";
			notification.payload = {
				type: "invitation",
				prayerId: self.id,
				invitationsCount: invitationsCount
			};
			notification.clickAction = "invitation";
			// console.log("PRE participant.user.pushNotification(notification);");
			participant.user.pushNotification(notification);
			
			cb(null);
		}
	],
	function(error) {
		mainCb(error);
	});
}

prayerSchema.methods.scheduleCloseJob = function() {
	var closeDate = this.closeDate;
	logger.info("Schedule PrayerCloseJob: %s", closeDate.toString());
	agenda.schedule(closeDate, 'PrayerCloseJob', {prayerId: this.id});
}

prayerSchema.methods.scheduleNotificationJob = function(userId, mainCb) {
	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status == 'active')
	{
		var remindDate = moment(this.date);
		remindDate.subtract(participant.user.prayReminder, 'minutes');
		remindDate = remindDate.toDate();
		var nowDate = new Date();
		if(remindDate.getTime() > nowDate.getTime())
		{
			logger.info("Schedule PrayerNotificationJob: %s", remindDate.toString());
			agenda.schedule(remindDate, 'PrayerNotificationJob', {prayerId: this.id, participantId: participant.user.id});
		}
	}
	mainCb(null);
}

prayerSchema.methods.cancelNotificationJob = function(userId, mainCb) {
	agenda.cancel({name: 'PrayerNotificationJob', 'data.prayerId': this.id, 'data.participantId': userId}, function(error, numRemoved) {
		mainCb(error);
	});
}

prayerSchema.methods.remindParticipant = function(userId, mainCb) {	
	if(!this.hasParticipantSync(userId))
	{
		var error = new Error("User not a participant of this prayer");
		error.status = 400;
		return mainCb(error);
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status == 'active')
	{
		var notification = {};
		notification.alert =  'Prayer (' + this.prayerType + ') will start in ' + participant.user.prayReminder + ' min.';
		notification.sound = "default";
		notification.payload = {
			type: "reminder",
			prayerId: this.id
		};
		notification.clickAction = "reminder";
		participant.user.pushNotification(notification);
	}

	mainCb(null);
}

prayerSchema.methods.remindToReviewSpotSync = function(userId) {	
	if(!this.hasParticipantSync(userId) || !this.spotId)
	{
		return;
	}
	
	var participant = this.participantByUserIdSync(userId);
	if(participant.status != 'active')
	{
		return;
	}

	var notification = {};
	notification.alert =  'Please review spot';
	notification.sound = "default";
	notification.payload = {
		type: "review",
		prayerId: this.id,
		spotId: this.spotId
	};
	notification.clickAction = "review";
	participant.user.pushNotification(notification);
}


var Prayer = mongoose.model("Prayer", prayerSchema);

module.exports = Prayer;
