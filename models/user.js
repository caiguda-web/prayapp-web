var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var apn = require('../libs/apn');
var gcm = require('../libs/gcm');
var logger = require("../libs/logger");
var nodemailer = require("nodemailer");
var xoauth2 = require('xoauth2');
var clone = require('clone');
var moment = require('moment');

var locationSchema = new Schema({
	coordinate: {
		type: [Number],
		index: '2d'
	},
	
	date: {
		type: Date,
		required: true
	}
},
{
	_id: false
});

var tokenSchema = new Schema({
	token: String,
	date: Date,
	tokenType: {
		type: String,
		required: true,
		enum: ['ios', 'android']
	}
},
{
	_id: false
});

var userSchema = new Schema({
	
	email: {
		type: String,
		required: false,
		default: null,
		index: true
	},
	
	password: {
		type: String,
		required: true
	},
	
	name: {
		type: String,
		required: true
	},
	
	surname: {
		type: String,
		required: true
	},
	
	avatar: {
		type: String,
		required: false
	},
	
	gender: {
		type: Boolean,
		required: true,
	default: true
	},
	
	socialIdentifier: {
		type: String,
		required: false,
		default: null
	},
	
	socialType: {
		type: String,
		required: false,
		enum: ['FB', 'TW']
	},

	sect: {
		type: String,
		required: true,
		enum: ['sunni', 'shia', 'sufi', 'nopref']
	},
	
	prayReminder: {
		type: Number,
		enum: [5, 10, 30, 60],
		default: 5
	},
	
	searchRadius: {
		type: Number,
	default: 50000
	},
	
	messagesNotificationEnabled: {
		type: Boolean,
		required: false,
	default: true
	},
			
	pushTokens: [tokenSchema],
	
	location: locationSchema,
	
	signedIn: {
		type: Boolean,
		required: false,
	default: false
	},
	
	showOnTheMap: {
		type: Boolean,
		required: false,
	default: true
	}
	
});

userSchema.index({'location.coordinate': 1, sect: 1, showOnTheMap: 1});

userSchema.pre('remove', function(next) {
	User.removeFile(this.avatar);
	next(null);
});

userSchema.virtual('fullName').get(function() {
	return this.name + ' ' + this.surname;
});

userSchema.statics.all = function(mainCb) {
	this.find({}, '-__v -password', mainCb);
}

userSchema.statics.findByUserId = function(identifier, mainCb) {
	this.findById(identifier, '-__v -password', function(error, user) {
		if(!error && !user)
		{
			error = new Error("Can't find user with such identifier");
			error.status = 404;
		}
		mainCb(error, user);
	});
}

userSchema.statics.findByEmail = function(email, shouldExists, mainCb) {
	this.findOne({email: email.toLowerCase()}, '-__v', function(error, user) {
		if(!error && !user && shouldExists)
		{
			error = new Error("Can't find user with such identifier");
			error.status = 404;
		}
		mainCb(error, user);
	});
}

userSchema.statics.findBySocialId = function(identifier, shouldExists, mainCb) {
	this.findOne({socialIdentifier: identifier}, '-__v -password', function(error, user) {
		if(!error && !user && shouldExists)
		{
			error = new Error("Can't find user with such identifier");
			error.status = 404;
		}
		mainCb(error, user);
	});
}

userSchema.statics.removeUser = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			User.findByUserId(identifier, function(error, user) {
				cb(error, user);
			})
		},
		function(user, cb) {
				user.remove(function(error) {
					cb(error);
				});
		}
	],
	function(error) {
		mainCb(error);
	});
}

userSchema.statics.nearestUsers = function(sect, latitude, longitude, radius, showOnlyEnabled, mainCb) {	
	var degrees = radius / 110574.0; // Latitude: 1 deg = 110.574 km
	var area = {center:[longitude, latitude], radius: degrees};

	var query = User.find();
	query.where('location.coordinate').circle(area).select('_id avatar sect name surname gender location');
	// var query = sect == 'nopref' ? User.find() : User.find({$or: [{'sect': sect}, {'sect': 'nopref'}]});
	if(sect && sect != 'nopref')
		query.or([{'sect': sect}, {'sect': 'nopref'}]);
	
	if(showOnlyEnabled)
	{
		query.where({$or: [{showOnTheMap: true}, {showOnTheMap: {$exists: false}}]});
	}
	
	var updateDate = moment(new Date());
	updateDate.subtract(60, 'minutes');
	updateDate = updateDate.toDate();
	query.where('location.date').gt(updateDate);
	
	query.exec(function(error, users) {
		mainCb(error, users);
	});
}

userSchema.statics.signup = function(email, password, name, surname, gender, sect, avatar, socialIdentifier, socialType, mainCb) {	
	var self = this;
	async.waterfall([
		function(cb) {
			if(socialIdentifier)
				self.findBySocialId(socialIdentifier, false, cb);
			else
				self.findByEmail(email, false, cb);			
		},
		function(user, cb) {
			var error = null;
			if(user)
			{
				if(socialIdentifier)
				{
					error = new Error("User with such social identifier already exists.");
					error.status = 400;
				}
				else
				{
					error = new Error("User with such email already exists.");
					error.status = 400;
				}
			}
			cb(error);
		},
		function(cb) {
			if(avatar)
			{				
				var oldPath = avatar.path;
				var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
				var newPath = path.join(__dirname, '../public/images/users/' + fileName);
				// console.log("newPath: ", newPath);
				fs.move(oldPath, newPath, function(error) {
					cb(error, fileName);
				});
			}
			else
			{
				cb(null, null);
			}
		},
		function(fileName, cb) {
			var user = new User();
			user.email = email;
			user.encryptPasswordSync(password);
			user.name = name;
			user.surname = surname;
			user.gender = gender;
			user.sect = sect;
			user.avatar = fileName;
			if(socialIdentifier)
			{
				user.socialIdentifier = socialIdentifier;
				user.socialType = socialType;
			}
			user.signedIn = true;
		
			user.save(function(error, user) {
				if(error)
				{
					User.removeFile(fileName);
					logger.error("User: Can't save new entity: " + error);
				}
				cb(error, user);
			});
		},
		function(user, cb)
		{
			var html = "<h2>Hi " + user.name + ' ' + user.surname + "!</h2><br/><p>You successfully registered.</p><p>Welcome!</p>"
			user.sendMail('Jamaah: Registration successful', html, function(error) {
				cb(error, user);
			});
		}
	],
	function(error, user) {
		mainCb(error, user);
	});
}

userSchema.statics.signin = function(email, password, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			self.findByEmail(email, true, function(error, user) {
				cb(error, user);
			});
		},
		function(user, cb) {
			var error = null;
			if(!user.validatePasswordSync(password))
			{
				error = new Error("User password is wrong");
				error.status = 400;
				user = null;
			}
			cb(error, user);
		},
		function(user, cb) {
			user.signedIn = true;
			user.save(cb);
		}
	],
	function(error, user) {
		if(error)
		{
			error = new Error("Email or password is invalid.");
			error.status = 400;
		}
		mainCb(error, user);
	});
}

userSchema.statics.signinBySocialId = function(identifier, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			self.findBySocialId(identifier, true, cb);
		},
		function(user, cb) {
			user.signedIn = true;
			user.save(cb);
		}
	],
	function(error, user) {
		mainCb(error, user);
	});
}

userSchema.statics.restore = function(email, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			self.findByEmail(email, true, function(error, user) {
				cb(error, user);
			});
		},
		function(user, cb) {
			var restoreUrl = config.restoreUrl + "?id=" + user.id;
			console.log("Restore URL: ", restoreUrl);
			var html = "<h2>Hi " + user.name + ' ' + user.surname + " !</h2><br><p>To restore your password click <a href='" + restoreUrl + "'>here</a>.</p>";
			user.sendMail('Jamaah: Resore password', html, function(error) {
				cb(error);
			});
		}
	],
	function(error) {
		mainCb(error);
	});
}

userSchema.methods.signout = function(mainCb) {
	this.signedIn = false;
	this.save(function(error, user) {
		mainCb(error, null);
	});
}

userSchema.methods.updatePassword = function(password, mainCb) {
	this.encryptPasswordSync(password);
	this.save(function(error, user) {
		if(error)
		{
			logger.error("User: Can't save new entity: " + error);
		}
		mainCb(error, user);
	});
}

userSchema.methods.updateProfile = function(name, surname, gender, sect, prayReminder, searchRadius, messagesNotificationEnabled, showOnTheMap, mainCb) {
	this.name = name;
	this.surname = surname;
	this.gender = gender;
	this.sect = sect;
	this.prayReminder = prayReminder;
	this.searchRadius = searchRadius;
	this.messagesNotificationEnabled = messagesNotificationEnabled;
	this.showOnTheMap = showOnTheMap;

	this.save(function(error, user) {
		if(error)
		{
			logger.error("User: Can't save new entity: " + error);
		}
		mainCb(error, user);
	});
}

userSchema.methods.updateAvatar = function(avatar, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			var oldPath = avatar.path;
			var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
			var newPath = path.join(__dirname, '../public/images/users/' + fileName);
			// console.log("newPath: ", newPath);
			fs.move(oldPath, newPath, function(error) {
				cb(error, fileName);
			});
		},
		function(fileName, cb) {
			var oldAvatar = self.avatar;
			self.avatar = fileName;
			self.save(function(error, user) {
				if(error)
				{
					User.removeFile(fileName);
					logger.error("User: Can't save new entity: " + error);
				}
				else
				{
					User.removeFile(oldAvatar);
				}
				cb(error, user);
			});
		},
	],
	function(error, user) {
		mainCb(error, error ? null : user);
	});
}

userSchema.methods.updateLocation = function(latitude, longitude, mainCb) {
	this.location = {
		coordinate: [longitude, latitude],
		date: Date.now()
	};
	this.save(function(error, user) {
		if(error)
			logger.error("User: Can't save entity: " + error);
		mainCb(error, user);
	});
}

userSchema.methods.hasPushToken = function(pushToken) {
	var hasToken = false;
	for(var i = 0; i < this.pushTokens.length; ++i)
	{
		if(this.pushTokens[i].token === pushToken)
		{
			hasToken = true;
			break;
		}
	}
	return hasToken;
}

userSchema.methods.pushTokenIndex = function(pushToken, tokenType) {
	var index = -1;
	for(var i = 0; i < this.pushTokens.length; ++i)
	{
		if(this.pushTokens[i].token === pushToken && this.pushTokens[i].tokenType === tokenType)
		{
			index = i;
			break;
		}
	}
	return index;
}

userSchema.methods.registerPushToken = function(pushToken, tokenType, mainCb) {
	var tokenIndex = this.pushTokenIndex(pushToken, tokenType);
	if(tokenIndex == -1)
	{
		this.pushTokens.push({token: pushToken, date: new Date(), tokenType: tokenType});
	}
	else
	{
		this.pushTokens[tokenIndex].date = new Date();
	}
	this.save(function(error, user) {
		if(error)
			logger.error("User: Can't save entity: " + error);
		mainCb(error, user);
	});
}

userSchema.methods.removePushToken = function(pushToken, tokenType, date, mainCb) {
	var tokenIndex = this.pushTokenIndex(pushToken, tokenType);
	if((tokenIndex != -1) && (date == null || this.pushTokens[tokenIndex].date.getTime() <= date.getTime()))
	{
		this.pushTokens.splice(tokenIndex, 1);
		this.save(function(error, user) {
			if(error)
				logger.error("User: Can't save entity: " + error);
			mainCb(error, user, true);
		});
	}
	else
	{
		mainCb(null, this, false);
	}	
}

userSchema.methods.clearPushTokens = function(mainCb) {
	this.pushTokens = [];
	this.save(function(error, user) {
		if(error)
			logger.error("User: Can't save entity: " + error);
		mainCb(error, user);
	});
}

userSchema.methods.pushNotification = function(notification) {
	if(!this.signedIn)
		return;
	
	var self = this;
	async.each(self.pushTokens, function(pushToken, itCb) {
		if(pushToken.tokenType == 'ios')
		{
			self.pushNotificationiOS(notification, pushToken, itCb);
		}
		else if(pushToken.tokenType == 'android')
		{
			self.pushNotificationAndroid(notification, pushToken, itCb);
		}
		else
		{
			var error = new Error("Wrong token type");
			itCb(error);
		}
		
	}, function(error) {
		//
	});
}

userSchema.methods.pushNotificationiOS = function(notification, pushToken, cb) {
	var pushNotification = new apn.Notification();
	pushNotification.alert = clone(notification.alert);
	pushNotification.sound = clone(notification.sound);
	pushNotification.payload = clone(notification.payload);
	apn.connection.pushNotification(pushNotification, pushToken.token);	
	cb(null);
}

userSchema.methods.pushNotificationAndroid = function(notification, pushToken, cb) {
	var self = this;
	// console.log("Notification4:\n", notification);
	var message = new gcm.Message({
		notification: {
			title: "Jamaah",
			body: clone(notification.alert)
		},
		data: clone(notification.payload)
	});
	// console.log("Message: ", message);
	if(notification.clickAction)
	{
		message.addNotification('click_action', clone(notification.clickAction));
		message.addNotification('sound', 'default');
	}
	
	gcm.sender.send(message, {registrationTokens: [pushToken.token]}, function(error, response) {
		if(error)
		{
			logger.error("Can't send notification:" + error);
			cb(error);
		}
		else if(response.results[0].error === 'NotRegistered')
		{
			self.removePushToken(pushToken.token, 'android', null, function(error, user, removed) {
				if(error)
					logger.error("User: can't remove android token: %s Error: %s", pushToken.token, error);
				else if(removed)
					logger.info("User: android pushToken removed successfully: ", pushToken.token);
				else
					logger.info("User: android pushToken NOT removed: ", pushToken.token);
				cb(error);
			});
		}
		else
		{
			logger.info("GCM notification %s was transmitted to device %s", JSON.stringify(message), pushToken.token);
			cb(null);
		}		
	});	
}

userSchema.statics.removeFile = function(fileName) {
	if(fileName && fileName.length > 0)
	{
		var filePath = path.join(__dirname, '../public/images/users/' + fileName);
		fs.unlink(filePath, function(error) {
			if(error)
				logger.error("Can't remove old file:" + filePath + " | May be unexist. Error:" + error);
		});
	}
}

userSchema.methods.validatePasswordSync = function(password) {
	return bcrypt.compareSync(password, this.password);
}

userSchema.methods.encryptPasswordSync = function(password) {
	this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
	return this.password;
}

userSchema.methods.sendMail = function(subject, body, mainCb) {
	if(!this.email)	// if user register via social network sometimes we can't receive his email
		return mainCb(null);
	
	var mailOptoins = {
		from: config.mailer.user,
		to: this.email,
		subject: subject,
		html: body
	};
	console.log("mailOptoins: ", mailOptoins);

	var oauthGenerator = xoauth2.createXOAuth2Generator({
		user: config.mailer.user,
		clientId: config.mailer.clientId,
		clientSecret: config.mailer.clientSecret,
		refreshToken: config.mailer.refreshToken,
		accessToken: config.mailer.accessToken
	});
	var transporter = nodemailer.createTransport({
	    service: 'gmail',
	    auth: {
				xoauth2: oauthGenerator
	    }
	});
	transporter.sendMail(mailOptoins, function(error, info) {
		if(error)
			logger.error("User: Failed to send restore email: ", error.message);
		else
			logger.info("User: Restore email sent to email: %s", this.email);
		transporter.close();
		mainCb(error);
	});
}


var User = mongoose.model("User", userSchema);

module.exports = User;


//  APN FEEDBACK example:  { time: 1460568750, device: { token: <Buffer 4c cb 14 21 15 0a 7e cf d6 fe 92 a4 cf 9d 58 dc 34 70 c5 3f a7 68 14 78 89 40 3a 8b 2f 7f 64 57> } }

apn.feedback.on('feedback', function(devices) {
	console.log("APN FEEDBACK: ", devices);

	devices.forEach(function(item) {
		var token = item.device.token.toString();
		var date = new Date(1000 * item.time);
		User.findOne({pushTokens: {$elemMatch: {token: token}}}, function(error, user) {
			if(error)
			{
				logger.error("Failed to find user by token: ", error);
			}
			else if(user)
			{
				console.log("APN FEEDBACK: ", item);
				user.removePushToken(token, 'ios', date, function(error, user, removed) {
					if(error)
						logger.error("User: can't remove token: ", error);
					else if(removed)
						logger.info("User: pushToken removed successfully");
					else
						logger.info("User: pushToken NOT removed");
				});
			}
		});
	});
});

// apn.connection.on('transmissionError', function(errorCode, notification, device) {
// 	logger.info("APN transmissionError: %s for notification %s for device %s", errorCode, JSON.stringify(notification), device.toString());
//
// 	if(errorCode == 8)
// 	{
// 		var token = device.toString();
// 		User.findOne({pushTokens: {$elemMatch: {token: token}}}, function(error, user) {
// 			if(error)
// 			{
// 				logger.error("Failed to find user by token: ", error);
// 			}
// 			else if(user)
// 			{
// 				user.removePushToken(token, 'ios', null, function(error, user, removed) {
// 					if(error)
// 						logger.error("User: can't remove token: ", error);
// 					else if(removed)
// 						logger.info("User: pushToken removed successfully");
// 					else
// 						logger.info("User: pushToken NOT removed");
// 				});
// 			}
// 		});
// 	}
// });
