var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var async = require('async');
var config = require('config');
var path = require('path');
var fs = require("fs-extra");
var Crypto = require('crypto');
var moment = require('moment');
var logger = require("../libs/logger");
var Admin = require("./admin");
var User = require("./user");
var ReportedSpot = require("./reportedSpot");
var VisitedSpot = require("./visitedSpot");
var Prayer = require("./prayer");

var LocationSchema = require("./locationSchema");

var reviewSchema = new Schema({
	noiseRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	crowdRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	cleanRate: {
		type: Number,
		min: 1,
		max: 5		
	},
	
	locationRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	comment: {
		type: String,
		maxlength: 1024,
		required: false,
		default: null
	},
	
	reviewer: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
		index: true
	},
	
	created: {
		type: Date,
		required: true
	}	
},
{
	_id: false
});

reviewSchema.virtual('rate').get(function() {
	return (this.noiseRate + this.crowdRate + this.cleanRate + this.locationRate) / 4.0;
});

reviewSchema.virtual('createdFormatted').get(function() {
	return moment(this.created).format('DD MMM YYYY hh:mm A');
});

var spotSchema = new Schema({
	
	location: LocationSchema,
	
	description: {
		type: String,
		maxlength: 1024,
		required: false,
		default: null
	},
	
	photo: {
		type: String,
		required: false,
		default: null
	},
	
	owner: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
		index: true
	},
	
	created: {
		type: Date,
		required: true
	},

	reviews: [reviewSchema],
	
	noiseRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	crowdRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	cleanRate: {
		type: Number,
		min: 1,
		max: 5		
	},
	
	locationRate: {
		type: Number,
		min: 1,
		max: 5
	},
	
	averageRate: {
		type: Number,
		required: true,
		default: 0
	},

	toDelete: {
		type: Boolean,
		required: false,
		default: false
	}	
});

spotSchema.index({owner: 1});

spotSchema.virtual('createdFormatted').get(function() {
	return moment(this.created).format('DD MMMM YYYY - hh:mm A');
});

spotSchema.pre('remove', function(next) {
	Spot.removeFile(this.photo);
	ReportedSpot.findOne({spot: this.id}, '_id').exec(function(error, rs) { if(rs) {rs.remove();} });
	VisitedSpot.findOne({spot: this.id}, '_id').exec(function(error, vs) { if(vs) {vs.remove();} });
	Prayer.find({spotId: this.id}, '_id spotId').exec(function(error, prayers) {
		if(prayers)
		{
			prayers.forEach(function(p) {
				p.spotId = null;
				p.save();
			});
		}
	});
	next(null);
});

spotSchema.virtual('rate').get(function() {
	var rate = 0;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		rate += this.reviews[i].rate;
	}
	if(this.reviews.length > 1)
		rate = rate / this.reviews.length;
	
	return rate;
});

spotSchema.virtual('averageNoiseRate').get(function() {
	var rate = 0;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		rate += this.reviews[i].noiseRate;
	}
	if(this.reviews.length > 1)
		rate = rate / this.reviews.length;
	
	return rate;
});

spotSchema.virtual('averageCrowdRate').get(function() {
	var rate = 0;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		rate += this.reviews[i].crowdRate;
	}
	if(this.reviews.length > 1)
		rate = rate / this.reviews.length;
	
	return rate;
});

spotSchema.virtual('averageCleanRate').get(function() {
	var rate = 0;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		rate += this.reviews[i].cleanRate;
	}
	if(this.reviews.length > 1)
		rate = rate / this.reviews.length;
	
	return rate;
});

spotSchema.virtual('averageLocationRate').get(function() {
	var rate = 0;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		rate += this.reviews[i].locationRate;
	}
	if(this.reviews.length > 1)
		rate = rate / this.reviews.length;
	
	return rate;
});

spotSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v -reviews');
	query.exec(function(error, spots) {
		mainCb(error, spots);
	});
}

spotSchema.statics.findBySpotId = function(identifier, mainCb) {
	var query = this.findById(identifier, '-__v');
	query.populate("owner", "_id name surname avatar sect signedIn gender");
	query.populate("reviews.reviewer", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, spot) {
		if(!error && !spot)
		{
			error = new Error("Can't find spot with such identifier");
			error.status = 404;
		}
		mainCb(error, spot);
	});	
}

spotSchema.statics.findBySpotIds = function(ids, mainCb) {
	var query = this.find({_id: {$in: ids}}, '-__v -reviews');
	query.populate("owner", "_id name surname avatar sect signedIn gender");
	// query.populate("reviews.reviewer", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, spots) {
		mainCb(error, spots);
	});	
}

spotSchema.statics.findByOwnerId = function(ownerId, mainCb) {
	var query = this.find({$and: [{owner: ownerId}, {$or: [{toDelete: false}, {toDelete: {$exists: false}}]}]}, '-__v -reviews');
	query.populate("owner", "_id name surname avatar sect signedIn gender");
	// query.populate("reviews.reviewer", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, spots) {
		if(!error && !spots)
		{
			error = new Error("Can't find spot(s) with such identifier");
			error.status = 404;
		}
		mainCb(error, spots);
	});	
}

spotSchema.statics.findByReviewerId = function(reviewerId, mainCb) {
	var query = this.find({reviews: {$elemMatch: {reviewer: mongoose.Types.ObjectId(reviewerId)}}}, '-__v -reviews');
	// query.sort({"created": -1});
	query.populate("owner", "_id name surname avatar sect signedIn gender");
	// query.populate("reviews.reviewer", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, spots) {
		mainCb(error, spots);
	});
}

spotSchema.statics.nearestSpots = function(latitude, longitude, radius, mainCb) {
	var degrees = radius / 110574.0; // Latitude: 1 deg = 110.574 km
	var area = {center:[longitude, latitude], radius: degrees};
	var query = Spot.find();
	query.where('location.coordinate').circle(area).select('-__v -reviews');
	query.populate("owner", "_id name surname avatar sect signedIn gender");
	// query.populate("reviews.reviewer", "_id name surname avatar sect signedIn gender");
	query.exec(function(error, spots) {
		mainCb(error, spots);
	});
}

spotSchema.statics.createSpot = function(latitude, longitude, address, description, ownerId, mainCb) {
	async.waterfall([
		function(cb) {
			User.findByUserId(ownerId, cb);
		},
		function(user, cb){
			var spot = new Spot();
			spot.location = {
				coordinate:[longitude, latitude],
				address: address ? address : null
			};
			spot.description = description;
			spot.photo = null;
			spot.owner = user.id;
			spot.reviews = [];
			spot.created = new Date();
			spot.save(function(error, spot) {
				if(error)
					logger.error("Spot: Can't save new entity: " + error);
				cb(error, spot);
			});
		},
		function(spot, cb) {
			Spot.findBySpotId(spot.id, cb);
		}
	],
	function(error, spot) {
		mainCb(error, spot);
	});
}

spotSchema.statics.removeSpot = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(identifier, function(error, spot) {
				cb(error, spot);
			})
		},
		function(spot, cb) {
			Prayer.findPrayers(null, null, spot.id, function(error, prayers) {
				cb(error, spot, prayers);
			});
		},
		function(spot, prayers, cb) {
			if(prayers.length == 0)
			{
				spot.remove(cb);
			}
			else
			{
				spot.toDelete = true;
				spot.save(cb);
			}
		}
	],
	function(error) {
		if(error && error.status == 404)
			error = null
		mainCb(error);
	});
}

spotSchema.statics.removeFile = function(fileName) {
	if(fileName && fileName.length > 0)
	{
		var filePath = path.join(__dirname, '../public/images/spots/' + fileName);
		fs.unlink(filePath, function(error) {
			if(error)
				logger.error("Can't remove old file:" + filePath + " | May be unexist. Error:" + error);
		});
	}
}

spotSchema.methods.updatePhoto = function(photo, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			var oldPath = photo.path;
			var fileName = Crypto.randomBytes(20).toString('hex') + path.extname(oldPath);
			var newPath = path.join(__dirname, '../public/images/spots/' + fileName);
			fs.move(oldPath, newPath, function(error) {
				cb(error, fileName);
			});
		},
		function(fileName, cb) {
			var oldPhoto = self.photo;
			self.photo = fileName;
			self.save(function(error, spot) {
				if(error)
				{
					Spot.removeFile(fileName);
					logger.error("Spot: Can't save new entity: " + error);
				}
				else
				{
					Spot.removeFile(oldPhoto);
				}
				cb(error, spot);
			});
		},
	],
	function(error, spot) {
		mainCb(error, error ? null : spot);
	});
}

spotSchema.methods.updateLocation = function(latitude, longitude, address, mainCb) {
	this.location = {
		coordinate: [longitude, latitude],
		address: address
	};
	this.save(function(error, spot) {
		if(error)
			logger.error("Spot: Can't save entity: " + error);
		mainCb(error, error ? null : spot);
	});
}

spotSchema.methods.updateDescription = function(description, mainCb) {
	this.description = description;
	this.save(function(error, spot) {
		if(error)
			logger.error("Spot: Can't save entity: " + error);
		mainCb(error, error ? null : spot);
	});
}

spotSchema.methods.wasReviewedByUser = function(userId) {
	var wasReviewed = false;
	for(var i = 0; i < this.reviews.length; ++i)
	{
		if(this.reviews[i].reviewer.id === userId)
		{
			wasReviewed = true;
			break;
		}
	}
	return wasReviewed;
}

spotSchema.methods.addReview = function(noiseRate, crowdRate, cleanRate, locationRate, comment, reviewerId, mainCb) {
	// if(this.wasReviewedByUser(reviewerId))
	// {
	// 	var error = new Error("User with such identifier has already reviewed this spot!");
	// 	error.status = 400;
	// 	return mainCb(error, null);
	// }
		
	var review = {
		noiseRate: noiseRate,
		crowdRate: crowdRate,
		cleanRate: cleanRate,
		locationRate: locationRate,
		comment: comment,
		reviewer: reviewerId,
		created: new Date()
	};
	
	for(var i = 0; i < this.reviews.length; ++i)
	{
		if(this.reviews[i].reviewer.id === reviewerId)
		{
			this.reviews.splice(i, 1);
			break;
		}
	}
	this.reviews.push(review);
	
	this.noiseRate = this.averageNoiseRate;
	this.crowdRate = this.averageCrowdRate;
	this.cleanRate = this.averageCleanRate;
	this.locationRate = this.averageLocationRate;
	this.averageRate = this.rate;
	
	this.save(function(error, spot) {
		if(error)
			logger.error("Spot: Can't save entity: " + error);
		mainCb(error, error ? null : spot);
	});
}

spotSchema.methods.removeReview = function(reviewerId, mainCb) {
	for(var i = 0; i < this.reviews.length; ++i)
	{
		if(this.reviews[i].reviewer.id === reviewerId)
		{
			this.reviews.splice(i, 1);
			break;
		}
	}
	this.noiseRate = this.averageNoiseRate;
	this.crowdRate = this.averageCrowdRate;
	this.cleanRate = this.averageCleanRate;
	this.locationRate = this.averageLocationRate;
	this.averageRate = this.rate;
	this.save(function(error, spot) {
		if(error)
			logger.error("Spot: Can't save entity: " + error);
		mainCb(error, error ? null : spot);
	});
}

var Spot = mongoose.model("Spot", spotSchema);


module.exports = Spot;
