var mongoose = require("../libs/mongoose");
var Schema = mongoose.Schema;
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var logger = require("../libs/logger");
var async = require('async');

var adminSchema = new Schema({
	username: {
		type: String,
		unique: true
	},
	
	password: {
		type: String,
		required: true
	},
	
	email: {
		type: String,
		required: false
	},
	
	role: {
		type: String,
		required: true,
		enum: ['master', 'locationManager']
	}
});

adminSchema.index({role: 1, username: 1});

adminSchema.statics.all = function(mainCb) {
	var query = this.find({}, '-__v -password');
	query.exec(function(error, admins) {
		mainCb(error, admins);
	});
}

adminSchema.statics.findByAdminId = function(identifier, mainCb) {
	this.findById(identifier, '-__v', function(error, admin) {
		if(!error && !admin)
		{
			error = new Error("Can't find admin with such identifier");
			error.status = 404;
		}
		mainCb(error, admin);
	});
}

adminSchema.statics.findByEmail = function(email, shouldExists, mainCb) {
	this.findOne({email: email.toLowerCase()}, '-__v -password', function(error, admin) {
		if(!error && !admin && shouldExists)
		{
			error = new Error("Can't find admin with such email");
			error.status = 404;
		}
		mainCb(error, admin);
	});
}

adminSchema.statics.findByUsername = function(username, shouldExists, mainCb) {
	this.findOne({username: username}, '-__v -password', function(error, admin) {
		if(!error && !admin && shouldExists)
		{
			error = new Error("Can't find admin with such username");
			error.status = 404;
		}
		mainCb(error, admin);
	});
}

adminSchema.statics.findLocationManagersByUsername = function(username, mainCb) {
	this.find({role: 'locationManager', username: new RegExp(username, 'i')}, '-__v -password', {sort: {username: 1}}, function(error, admins) {
		mainCb(error, admins);
	});
}

adminSchema.methods.validatePasswordSync = function(password) {
	return bcrypt.compareSync(password, this.password);
}

adminSchema.methods.encryptPasswordSync = function(password) {
	this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
	return this.password;
}

adminSchema.statics.createAdmin = function(username, password, email, role, mainCb) {
	var self = this;
	async.waterfall([
		function(cb) {
			self.findByUsername(username, false, cb);
		},
		function(admin, cb) {
			var error = null;
			if(admin)
			{
				var error = new Error("Admin with such username already exists.");
				error.status = 400;
			}
			cb(error);
		},
		function(cb) {
			var admin = new Admin();
			admin.username = username;
			admin.encryptPasswordSync(password);
			admin.email = email ? email : null;
			admin.role = role;
			admin.save(function(error, admin) {
				if(error)
				{
					logger.error("Can't save admin: ", error);
				}
				cb(error, admin);
			});	
		}
	], 
	function(error, admin) {
		mainCb(error, admin);
	});
}

adminSchema.statics.removeAdmin = function(identifier, mainCb) {
	async.waterfall([
		function(cb) {
			Admin.findByAdminId(identifier, cb)
		},
		function(admin, cb) {
			admin.remove(cb);
		}
	],
	function(error) {
		mainCb(error);
	});
}


var Admin = mongoose.model('Admin', adminSchema);


passport.serializeUser(function(admin, done) {
  done(null, admin._id);
});

passport.deserializeUser(function(id, done) {
  Admin.findByAdminId(id, function(error, admin) {
    done(error, admin);
  });
});


module.exports = Admin;
