module.exports = function(req, res, next) {

	res.normSend = function(code, data, errorMessage) {
		var result = {code: code, data: data};
		if(errorMessage)
		{
			 result.error = {message: errorMessage}
		}
		else
		{
			result.error = null;
		}
		res.send(result);
	};
	
	next();
};