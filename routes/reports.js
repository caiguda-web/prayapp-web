var express = require('express');
var router = express.Router();
var util = require('util');
var moment = require('moment');
var async = require('async');
var logger = require('../libs/logger');
var passport = require('../libs/passport');
var ObjectID = require('../libs/mongoose').ObjectID;
var routerHelper = require('../libs/routerHelper');
var ReportedSpot = require('../models/reportedSpot');
var Spot = require('../models/spot');

module.exports = router;


router.get("/page/:index", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.reports)
		req.session.reports = {filter: {}};

	req.session.reports.page = req.params.index;

	res.redirect('/reports');
});


router.get("/sort/:type/:order", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.reports)
		req.session.reports = {filter: {}};

	req.session.reports.sortType = req.params.type;
	req.session.reports.sortOrder = req.params.order;

	res.redirect('/reports');
});


router.post("/filter", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.reports)
		req.session.reports = {filter: {}};

	req.session.reports.filter.owner = req.body.owner || '';
	req.session.reports.filter.address = req.body.address || '';
	req.session.reports.filter.reviewsCount = req.body.reviewsCount || '';
	req.session.reports.filter.rate = req.body.rate || '';
	req.session.reports.filter.reportsCount = req.body.reportsCount || '';
	req.session.reports.page = 1;

	res.redirect('/reports');
});


router.get("/filter/clear", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.reports)
		req.session.reports = {filter: {}};

	req.session.reports.filter = {};

	res.redirect('/reports');
});


router.get("/delete", passport.isAdminAuthenticated, function(req, res) {
	ReportedSpot.removeReportedSpot(req.query.id, function(error) {
		res.redirect('/reports');
	});
});


router.get("/deletespot", passport.isAdminAuthenticated, function(req, res) {
	async.waterfall([
		function(cb) {
			ReportedSpot.removeReportedSpot(req.query.reportId, cb);
		},
		function(cb) {
			Spot.removeSpot(req.query.spotId, cb);			
		}
	],
	function(error) {
		if(error)
		{
			req.flash('message', 'Has failed with error: ' + error.message);
		}
		res.redirect('/reports');		
	});
});


router.get("/", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.reports)
		req.session.reports = {filter: {}};
	
	var aggregation = [
		{
			$match: {
			}
		},
		{
			$lookup: {
				from: 'spots',
				localField: 'spot',
				foreignField: '_id',
				as: 'spots'
			}
		},
		{
			$project: {
				_id: 1,
				created: 1,
				spot: {$arrayElemAt: ['$spots', 0]},
				reportsCount: {$size: '$reports'}
			}
		},
		{
			$lookup: {
				from: 'users',
				localField: 'spot.owner',
				foreignField: '_id',
				as: 'owners'
			}
		},
		{
			$unwind: "$owners"
		},
		{
			$project: {
				_id: 1,
				created: 1,
				reportsCount: 1,
				spot: {
					_id: 1,
					created: 1,
					owner: {_id: '$owners._id', name: '$owners.name', surname: '$owners.surname', 
									fullName: { $concat: [ '$owners.name', ' ', '$owners.surname' ] } },
					location: 1,
					averageRate: 1,
					reviewsCount: {$size: '$spot.reviews'},
					photo: 1,
					description: 1
				}
			}
		}
	];

	// filter
	var filterOwner = req.session.reports.filter.owner || '';
	var filterAddress = req.session.reports.filter.address || '';
	var filterReviewsCount = req.session.reports.filter.reviewsCount || '';
	var filterRate = req.session.reports.filter.rate || '';
	var filterReportsCount = req.session.reports.filter.reportsCount || '';
	if(filterOwner.length)
	{
		aggregation.push({
								$match: {
									$or: [
										{'spot.owner.name': {$regex: filterOwner, $options:'i'}},
										{'spot.owner.surname': {$regex: filterOwner, $options:'i'}}
								]}});
	}
	if(filterAddress.length)
	{
		aggregation.push({ $match: { 'spot.location.address': { $regex: filterAddress, $options:'i' } } });
	}
	if(filterReviewsCount.length)
	{
		var value = parseInt(filterReviewsCount) || 0;
		aggregation.push({ $match: { 'spot.reviewsCount': { $gte: value } } });
	}
	if(filterRate.length)
	{
		var value = parseFloat(filterRate) || 0;
		aggregation.push({ $match: { 'spot.averageRate': { $gte: value } } });
	}
	if(filterReportsCount.length)
	{
		var value = parseInt(filterReportsCount) || 0;
		aggregation.push({ $match: { 'reportsCount': { $gte: value } } });
	}
	
	// sort
	var sortType = req.session.reports.sortType || 'owner';
	var sortOrder = req.session.reports.sortOrder == -1 ? -1 : 1;

	var sortOptions;
	if (sortType == 'owner')
		sortOptions = {'spot.owner.fullname': sortOrder};
	else if (sortType == 'address')
		sortOptions = {'spot.location.address': sortOrder};
	else if (sortType == 'reviewsCount')
		sortOptions = {'spot.reviewsCount': sortOrder};
	else if (sortType == 'rate')
		sortOptions = {'spot.averageRate': sortOrder};
	else if (sortType == 'reportsCount')
		sortOptions = {'spot.reportsCount': sortOrder};
	else
	{
		sortType = 'owner'
		sortOptions = {'spot.owner.fullname': sortOrder};
	}
	
	// pagination
	var limit = 20;
	var page = req.session.reports.page || 1;	
	routerHelper.paginatorWithAggregation(ReportedSpot, aggregation, sortOptions, page, limit, req.session.reports, function(error, models, pagesCount) {
		if(error)
		{
			logger.error("Can't get ReportedSpot during pagination: ", error);
			throw error;
		}
		else
		{
			var options = { admin: req.user,
											 page: 'reports', 
										message: req.flash('message'), 
						 	reportedSpots: models,
									 		 sort: {type: sortType, order: sortOrder},
								 pagination: {page: req.session.reports.page, count: pagesCount},
										 filter: req.session.reports.filter
			};
			// console.log(models);
		  res.render('reportsView', options);
		}
	});	
});


router.get("/report/:reportId", passport.isAdminAuthenticated, function(req, res) {
	ReportedSpot.findFullByReportedSpotId(req.params.reportId, function(error, reportedSpot) {
		if(error)
		{
			req.flash('message', 'Has failed with error: ' + error.message);
			res.redirect('/reports');
		}
		else
		{
			var options = { admin: req.user,
											 page: 'reports',
												tab: 'spot',
										message: req.flash('message'),
							 reportedSpot: reportedSpot
			};
		  res.render('reportView', options);
		}
	});
});


router.get("/*", passport.isAdminAuthenticated, function(req, res) {
	res.redirect('/reports');
});
