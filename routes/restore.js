var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var logger = require('../libs/logger');
var passport = require('../libs/passport');
var User = require('../models/user');
var ObjectID = require('../libs/mongoose').ObjectID;

module.exports = router;

router.get("/", function(req, res) {
	User.findByUserId(req.query.id, function(error, user) {
		var options = {page: 'restore',
									user: user,
								message: error ? "Such user doesn't exist" : req.flash('message')
									};
	  res.render('restoreView', options);
	});
});

router.get("/success/:userId", function(req, res) {
	User.findByUserId(req.params.userId, function(error, user) {
		var options = {page: 'restore',
									user: user,
								message: req.flash('message')
									};
	  res.render('restoreSuccessView', options);
	});
});

router.post("/", function(req, res) {
	var userId = req.body.id;
	var pas1 = req.body.password1;
	var pas2 = req.body.password2;
	if(pas1 === pas2)
	{
		async.waterfall([
			function(cb) {
				User.findByUserId(userId, cb);
			},
			function(user, cb) {
				user.updatePassword(pas1, cb);
			},
		],
		function(error, user) {
			if(error)
			{
				req.flash('message', "Failed to restore password");
				res.redirect('/restore/' + userId);
			}
			else
			{
				res.redirect('/restore/success/' + userId);
			}
		});
	}
	else
	{
		req.flash('message', "Passwords don't equal");
		res.redirect('/restore/' + userId);
	}
});
