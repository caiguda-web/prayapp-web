var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var logger = require('../libs/logger');
var passport = require('../libs/passport');
var ObjectID = require('../libs/mongoose').ObjectID;
var routerHelper = require('../libs/routerHelper');
var Admin = require('../models/admin');

module.exports = router;


router.get("/page/:index", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.managers)
		req.session.managers = {};

	req.session.managers.page = req.params.index;

	res.redirect('/managers');
});


router.get("/sort/:type/:order", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.managers)
		req.session.managers = {};

	req.session.managers.sortType = req.params.type;
	req.session.managers.sortOrder = req.params.order;

	res.redirect('/managers');
});


router.post("/filter", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.managers)
		req.session.managers = {};

	req.session.managers.filterUsername = req.body.username || '';
	req.session.managers.page = 1;

	res.redirect('/managers');
});


router.get("/filter/clear", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.managers)
		req.session.managers = {};

	req.session.managers.filterUsername = '';

	res.redirect('/managers');
});


router.get("/delete", passport.isAdminAuthenticated, function(req, res) {
	Admin.removeAdmin(req.query.id, function(error) {
		res.redirect('/managers');
	});
});


router.get("/", passport.isAdminAuthenticated, function(req, res) {
	if (!req.session.managers)
		req.session.managers = {};

	// sort
	var sortType = req.session.managers.sortType || 'username';
	var sortOrder = req.session.managers.sortOrder == -1 ? -1 : 1;

	var sortOptions;
	if (sortType == 'username')
		sortOptions = {username: sortOrder};
	else
	{
		sortType = 'username'
		sortOptions = {username: sortOrder};
	}

	// filter
	var filterUsername = req.session.managers.filterUsername || '';

	// query
	var query = Admin.find({role: {$ne: 'master'}}, null, {collation: {locale: 'en_US', strength: 3}});
	if(filterUsername.length)
	{
		var reg = new RegExp(filterUsername, 'i');
		query.where('username').in([reg]);
	}

	// pagination
	var limit = 20;
	var page = req.session.managers.page || 1;
	routerHelper.paginator(query, sortOptions, page, limit, req.session.managers, function(error, models, pagesCount) {
		if(error)
		{
			logger.error("Can't get managers during pagination: ", error);
			throw error;
		}
		else
		{
			var options = { admin: req.user,
 											 page: 'managers',
										message: req.flash('message'), 
									 managers: models,
									 		 sort: {type: sortType, order: sortOrder},
								 pagination: {page: page, count: pagesCount},
										 filter: {username: filterUsername}
			};
		  res.render('managersView', options);
		}
	});
});


// Ajax validation of manager
router.get("/manager/validate", function(req, res) {
	// console.log(req.query);
	if(req.query.username)
	{
		Admin.findOne({username: req.query.username}, '_id', function(error, manager) {
			if(!manager && !error)
			{
				res.sendStatus(200);
			}
			else
			{
				res.writeHead(400, 'Manager with such username already exists');
				res.send();
			}
		});
	}
	else
	{
		res.writeHead(400, 'Invalid field to validate');
		res.send();
	}
});


router.get("/manager/", passport.isAdminAuthenticated, function(req, res) {
	var options = { admin: req.user,
									 page: 'managers',
								message: req.flash('message'),
								manager: null
	};
  res.render('managerView', options);
});


router.get("/manager/:managerId", passport.isAdminAuthenticated, function(req, res) {
	Admin.findByAdminId(req.params.managerId, function(error, manager) {
		if(error)
		{
			req.flash('message', 'Has failed with error: ' + error.message);
			res.redirect('/managers');
		}
		else if (!manager)
		{
			req.flash('message', "Such manager doesn't exist");
			res.redirect('/managers');
		}
		else
		{
			var options = { admin: req.user,
											 page: 'managers',
										message: req.flash('message'),
										manager: manager
			};
		  res.render('managerView', options);
		}
	});
});


router.post("/manager", passport.isAdminAuthenticated, function(req, res) {
	var id = req.body.id;
	if(!id)
	{	
		var manager = Admin.createAdmin(req.body.username, req.body.password, null, 'locationManager', function(error, manager) {
			if(error)
			{
				req.flash('message', 'Manager creation has failed with error: ' + error.message);
				res.redirect('/managers/manager');
			}
			else
			{
				res.redirect('/managers');
			}
		});
	}
	else
	{
		async.waterfall([
			function(cb) {
				Admin.findByAdminId(id, function(error, manager) {
					cb(error, manager);
				});
			},
			function(manager, cb) {
				manager.encryptPasswordSync(req.body.password);
				manager.save(function(error) {
					cb(error);
				});
			}
		],
		function(error) {
			if(error)
			{
				req.flash('message', 'Manager edition has failed with error: ' + error.message);
			}
			res.redirect('/managers');
		});
	}
});


router.get("/*", passport.isAdminAuthenticated, function(req, res) {
	res.redirect('/managers');
});
