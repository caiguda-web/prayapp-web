var express = require('express');
var router = express.Router();
var util = require('util');
var moment = require('moment');
var async = require('async');
var logger = require('../libs/logger');
var passport = require('../libs/passport');
var ObjectID = require('../libs/mongoose').ObjectID;
var routerHelper = require('../libs/routerHelper');
var PrayerLocation = require('../models/prayerLocation');

module.exports = router;


router.get("/", passport.isAdminAuthenticated, function(req, res) {
	var options = { admin: req.user,
									 page: 'locations', 
								subpage: 'locationsMap',
								message: ''
	};
  res.render('locationsMapView', options);		
});


router.get("/*", passport.isAdminAuthenticated, function(req, res) {
	res.redirect('/locationsMap');
});
