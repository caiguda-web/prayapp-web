var express = require('express');
var router = express.Router();
var util = require('util');
var moment = require('moment');
var async = require('async');
var logger = require('../libs/logger');
var passport = require('../libs/passport');
var ObjectID = require('../libs/mongoose').ObjectID;
var routerHelper = require('../libs/routerHelper');
var PrayerLocation = require('../models/prayerLocation');

module.exports = router;


router.get("/page/:index", passport.isAuthenticated, function(req, res) {
	if (!req.session.locations)
		req.session.locations = {filter: {}};

	req.session.locations.page = req.params.index;

	res.redirect('/locations');
});


router.get("/sort/:type/:order", passport.isAuthenticated, function(req, res) {
	if (!req.session.locations)
		req.session.locations = {filter: {}};

	req.session.locations.sortType = req.params.type;
	req.session.locations.sortOrder = req.params.order;

	res.redirect('/locations');
});


router.post("/filter", passport.isAuthenticated, function(req, res) {
	if (!req.session.locations)
		req.session.locations = {filter: {}};

	req.session.locations.filter.name = req.body.name || '';
	req.session.locations.filter.placeType = req.body.placeType || '';
	req.session.locations.filter.address = req.body.address || '';
	req.session.locations.filter.manager = req.body.manager || '';
	req.session.locations.page = 1;

	res.redirect('/locations');
});


router.get("/filter/clear", passport.isAuthenticated, function(req, res) {
	if (!req.session.locations)
		req.session.locations = {filter: {}};

	req.session.locations.filter = {};

	res.redirect('/locations');
});


router.get("/delete", passport.isAdminAuthenticated, function(req, res) {
	PrayerLocation.removePrayerLocation(req.query.id, function(error) {
		res.redirect('/locations');
	});
});


router.get("/", passport.isAuthenticated, function(req, res) {
	if (!req.session.locations)
		req.session.locations = {filter: {}};

	// sort
	var sortType = req.session.locations.sortType || 'name';
	var sortOrder = req.session.locations.sortOrder == -1 ? -1 : 1;

	var sortOptions;
	if (sortType == 'name')
		sortOptions = {name: sortOrder};
	else if (sortType == 'placeType')
		sortOptions = {placeType: sortOrder};
	else if (sortType == 'manager')
		sortOptions = {'managerUsername': sortOrder};
	else if (sortType == 'address')
		sortOptions = {'location.address': sortOrder};
	else
	{
		sortType = 'name'
		sortOptions = {name: sortOrder};
	}
	
	// filter
	var filterName = req.session.locations.filter.name || '';
	var filterPlaceType = req.session.locations.filter.placeType || '';
	// var filterTimeOpen = req.session.locations.filter.timeOpen || '';
	// var filterTimeClose = req.session.locations.filter.timeClose || '';
	var filterAddress = req.session.locations.filter.address || '';
	var filterManager = req.session.locations.filter.manager || '';

	// query
	var queryParam = (req.user.role == 'master') ? {} : {managerUsername: req.user.username};
	var query = PrayerLocation.find(queryParam, null, {collation: {locale: 'en_US', strength: 3}});
	// var query = PrayerLocation.find();
	query.populate('manager', '_id username');
	if(filterName.length)
	{
		var reg = new RegExp(filterName, 'i');
		query.where('name').in([reg]);
	}
	if(filterPlaceType.length)
	{
		var reg = new RegExp(filterPlaceType, 'i');
		query.where('placeType').in([reg]);
	}
	if(filterAddress.length)
	{
		var reg = new RegExp(filterAddress, 'i');
		query.where('location.address').in([reg]);
	}
	if(filterManager.length)
	{
		var reg = new RegExp(filterManager, 'i');
		query.where('managerUsername').in([reg]);
	}

	// pagination
	var limit = 20;
	var page = req.session.locations.page || 1;
	routerHelper.paginator(query, sortOptions, page, limit, req.session.locations, function(error, models, pagesCount) {
		if(error)
		{
			logger.error("Can't get prayerLocations during pagination: ", error);
			throw error;
		}
		else
		{
			var options = { admin: req.user,
											 page: 'locations',
										subpage: 'locationsList',
										message: req.flash('message'),
									 locations: models,
									 		 sort: {type: sortType, order: sortOrder},
								 pagination: {page: page, count: pagesCount},
										 filter: req.session.locations.filter
			};
			// console.log(models);
		  res.render('locationsView', options);
		}
	});
});


router.get("/location/", passport.isAdminAuthenticated, function(req, res) {
	var options = { admin: req.user,
									 page: 'locations', 
								subpage: 'locationsList',
										tab: 'general',
								message: req.flash('message'),
								location: null
	};
  res.render('locationView', options);
});


router.get("/location/:locationId", passport.isAuthenticated, function(req, res) {
	
	async.waterfall([
		function(cb) {
			if(req.user.role == 'master')
			{
				PrayerLocation.findByPrayerLocationId(req.params.locationId, cb);
			}
			else
			{
				PrayerLocation.findByIdAndManagerUsername(req.params.locationId, req.user.username, cb);
			}			
		}
	],
	function(error, prayerLocation){
		if(error)
		{
			req.flash('message', 'Has failed with error: ' + error.message);
			res.redirect('/locations');
		}
		else if (!prayerLocation)
		{
			req.flash('message', "Such location doesn't exist");
			res.redirect('/locations');
		}
		else
		{
			var options = { admin: req.user,
											 page: 'locations',
										subpage: 'locationsList',
												tab: 'general',
										message: req.flash('message'),
										location: prayerLocation
			};
		  res.render('locationView', options);
		}
	});	
});

router.get("/*", passport.isAuthenticated, function(req, res) {
	res.redirect('/locations');
});
