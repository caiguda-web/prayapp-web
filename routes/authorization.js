var config = require('config');
var express = require('express');
var router = express.Router();
var passport = require('../libs/passport');
var Admin = require('../models/admin');
var PrayerLocation = require('../models/prayerLocation');

router.get('/', function(req, res) {
	if (req.isAuthenticated())
	{
		res.redirect('/home');
	}
	else
	{
		res.redirect('/signin');
	}
});

router.get('/signin', passport.isNotAuthenticated, function(req, res) {
  res.render('signin', { message: req.flash('auth-message'), page: 'signin',
		 										 showRegistration: config.util.getEnv('NODE_ENV') == "development"});
});

router.get('/signup', passport.isNotAuthenticated, function(req, res) {
	  res.render('signup', { message: req.flash('auth-message'), page: 'signup'});
});

router.get('/home', passport.isAuthenticated, function(req, res) {
	if(req.user.role == 'master')
	{
		res.redirect('/managers');
	}
	else if(req.user.role == 'locationManager')
	{
		res.redirect('/locations');
		// PrayerLocation.findByManagerUsername(req.user.username, function(error, prayerLocation) {
		// 	if(error)
		// 	{
		// 		if(error.status == 404)
		// 		{
		// 			req.flash('auth-message', 'Such admin has no assigned location');
		// 			req.logout();
		// 			res.redirect('/signin');
		// 		}
		// 		else
		// 		{
		// 			req.flash('auth-message', error.message);
		// 			req.logout();
		// 			res.redirect('/signin');
		// 		}
		// 	}
		// 	else
		// 	{
		// 		res.redirect('/locations/location/' + prayerLocation.id);
		// 	}
		// });
	}
	else
	{
		req.logout();
		res.redirect('/signin');
	}
});

router.post('/signin', passport.authenticate('signin-local', {
	successRedirect: '/home',
	failureRedirect: '/',
	failureFlash: true
}));

router.post('/signup', passport.authenticate('signup-local', {
	successRedirect: '/home',
	failureRedirect: '/signup',
	failureFlash: true
}));

router.get('/signout', function(req, res) {
	req.logout();
	res.redirect('/');
});

module.exports = router;
