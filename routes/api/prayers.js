var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var moment = require('moment');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var config = require('config');
var Prayer = require('../../models/prayer');
var Spot = require('../../models/spot');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;

router.get("/", function(req, res) {
	var prayerType = req.query.prayerType ? req.query.prayerType : null;
	var sect = req.query.sect ? req.query.sect : null;
	var spotId = req.query.spot ? req.query.spot : null;
	if(prayerType || sect || spotId)
	{
		Prayer.findPrayers(prayerType, sect, spotId, function(error, prayers) {
			res.normSend(error ? error.status || 500 : 0, prayers, error ? error.message : null);
		});
	}
	else
	{
		Prayer.all(function(error, prayers) {
			res.normSend(error ? error.status || 500 : 0, prayers, error ? error.message : null);
		});
	}
});


router.get("/nearestPrayers", function(req, res) {
	var latitude = req.query.lat;
	var longitude = req.query.lon;
	var radius = req.query.radius;
	var sect = req.query.sect;
	var prayerType = req.query.prayerType;
	
	Prayer.nearestPrayers(prayerType, sect, latitude, longitude, radius, function(error, prayers) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayers, error ? error.message : null);
	});
});


// only for development
if (config.util.getEnv('NODE_ENV') === 'development') {
	router.get("/clear", function(req, res) {
		async.waterfall([
			function(cb) {
				Prayer.all(cb);
			},
			function(prayers, cb) {
				async.each(prayers, function(prayer, itCb) {
					prayer.remove(function(error) {
						itCb(error);
					});
				}, function(error) {
					cb(error);
				});
			}
		],
		function(error) {
			res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
		});
	});
}
//----------------------

router.get("/:prayerId", function(req, res) {
	Prayer.findByPrayerId(req.params.prayerId, function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, prayer, error ? error.message : null);
	});
});


router.get("/owner/:ownerId", function(req, res) {
	Prayer.findByOwnerId(req.params.ownerId, function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, prayer, error ? error.message : null);
	});
});


router.get("/participant/:userId", function(req, res) {
	var participantStatus = req.query.status;
	Prayer.findByParticipantId(req.params.userId, participantStatus, function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, prayer, error ? error.message : null);
	});
});


// POST prayers/
var prayerCreateOptions = {
	body: {
		'ownerId': {
			isOptional: false
		},
		'prayerType': {
			isOptional: false
		},
		'date': {
			isOptional: false
		},
		'closeDate': {
			isOptional: false
		},
		'sect': {
			isOptional: false
		},
		'participantIds': {
			isOptional: true
		},
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: false
		},
		'spotId': {
			isOptional: true
		}
	}
};

router.post("/", RequestChecker.requestChecker(prayerCreateOptions), function(req, res) {	
	var date = null;
	var closeDate = null;
	if(req.body.date && req.body.closeDate)
	{
		// date = moment.unix(req.body.date).toDate();
		// closeDate = moment.unix(req.body.closeDate).toDate();
		date = moment(req.body.date, 'YYYY-MM-DDTHH:mm:ss.Z').toDate();
		closeDate = moment(req.body.closeDate, 'YYYY-MM-DDTHH:mm:ss.Z').toDate();
		// console.log("Date: ", date);
	}
	
	Prayer.createPrayer(req.body.ownerId,
											req.body.prayerType,
											date, 
											closeDate,
											req.body.sect,
											req.body.participantIds,
											req.body.latitude,
											req.body.longitude,
											req.body.address,
											req.body.spotId,
	function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, prayer, error ? error.message : null);
	});
});


router.delete("/:prayerId", function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, function(error, prayer) {
				cb(error, prayer);
			});
		},
		function(prayer, cb) {
			if(prayer.spotId)
			{
				Spot.findBySpotId(prayer.spotId, function(error, spot) {
					if(error && error.status == 404)
						error = null;
					cb(error, prayer, spot);
				});
			}
			else
			{
				cb(null, prayer, null);
			}
		},
		function(prayer, spot, cb) {
			prayer.remove(function(error) {
				cb(error, spot);
			});
		},
		function(spot, cb) {
			if(spot && spot.toDelete)
			{
				Spot.removeSpot(spot.id, function(error) {
					cb(error);
				});
			}
			else
			{
				cb(null);
			}
		}
	],
	function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);		
	});
	// Prayer.removePrayer(req.params.prayerId, function(error) {
	// 	res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	// });
});


// POST prayers/location
var locationOptions = {
	body: {
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: true
		}
	}
};

router.post("/:prayerId/location", RequestChecker.requestChecker(locationOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, function(error, prayer) {
				cb(error, prayer);
			});			
		},
		function(prayer, cb) {
			prayer.updateLocation(req.body.latitude, req.body.longitude, req.body.address, cb);	
		}
	],
	function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayer, error ? error.message : null);		
	});
});


router.post("/:prayerId/photo", function(req, res) {
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				if(!error && !files.photo)
				{
					error = new Error("Photo wasn't provided");
					error.status = 400;
				}
				cb(error, files.photo);
			});
		},
		function(photo, cb) {
			Prayer.findByPrayerId(req.params.prayerId, function(error, prayer) {
				cb(error, prayer, photo);
			});			
		},
		function(prayer, photo, cb) {
			prayer.updatePhoto(photo, cb);	
		}
	],
	function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayer, error ? error.message : null);		
	});
});


router.post("/:prayerId/acceptInvitation/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, cb);
		},
		function(prayer, cb) {
			prayer.userAcceptInvitation(req.params.userId, cb);
		},
		function(prayer, cb) {
			Prayer.invitationsCount(req.params.userId, function(error, invitationsCount) {
				cb(error, prayer, invitationsCount);
			});			
		}
	],
	function(error, prayer, invitationsCount) {
		var data = error ? null : { prayer: prayer, invitationsCount: invitationsCount};
		res.normSend(error ? error.status || 500 : 0, data, error ? error.message : null);		
	});
});


router.post("/:prayerId/declineInvitation/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, cb);
		},
		function(prayer, cb) {
			prayer.userDeclineInvitation(req.params.userId, cb);
		},
		function(prayer, cb) {
			Prayer.invitationsCount(req.params.userId, function(error, invitationsCount) {
				cb(error, prayer, invitationsCount);
			});			
		}
	],
	function(error, prayer, invitationsCount) {
		var data = error ? null : { prayer: prayer, invitationsCount: invitationsCount};
		res.normSend(error ? error.status || 500 : 0, data, error ? error.message : null);		
	});
});


router.post("/:prayerId/leave/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, cb);
		},
		function(prayer, cb) {
			prayer.userLeave(req.params.userId, cb);
		}
	],
	function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayer, error ? error.message : null);		
	});
});


router.post("/:prayerId/join/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			Prayer.findByPrayerId(req.params.prayerId, cb);
		},
		function(prayer, cb) {
			prayer.userJoin(req.params.userId, cb);
		}
	],
	function(error, prayer) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayer, error ? error.message : null);		
	});
});


router.get("/invitationsCount/:userId", function(req, res) {
	Prayer.invitationsCount(req.params.userId, function(error, invitationsCount) {
		res.normSend(error ? error.status || 500 : 0, error ? null : invitationsCount, error ? error.message : null);
	});
});
