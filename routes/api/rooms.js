var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var config = require('config');
var moment = require('moment');
var Room = require('../../models/room');
var Prayer = require('../../models/prayer');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;


function processRooms(rooms, mainCb) {
	async.map(rooms, function(room, itCb) {
		async.waterfall([
			function(cb) {
				Prayer.infoByPrayerId(room.prayerId, function(error, prayer) {
					cb(error, room, prayer);
				});
			},
			function(room, prayer, cb) {
				room = room.toObject();
				delete room.prayerId;
				room.prayer = prayer;
				cb(null, room);
			}
		],
		function(error, room) {
			itCb(error, room);
		});
	},
	function(error, rooms) {
		mainCb(error, rooms);
	});
}

function processSingleRoom(room, mainCb) {
	async.waterfall([
		function(cb) {
			Prayer.infoByPrayerId(room.prayerId, function(error, prayer) {
				cb(error, room, prayer);
			});
		},
		function(room, prayer, cb) {
			room = room.toObject();
			delete room.prayerId;
			room.prayer = prayer;
			cb(null, room);
		}
	],
	function(error, room) {
		mainCb(error, room);
	});	
}


router.get("/", function(req, res) {	
	Room.all(function(error, rooms) {
		res.normSend(error ? error.status || 500 : 0, rooms, error ? error.message : null);
	});
});


// only for development
if (config.util.getEnv('NODE_ENV') === 'development') {
	router.get("/clear", function(req, res) {
		async.waterfall([
			function(cb) {
				Room.all(cb);
			},
			function(rooms, cb) {
				async.each(rooms, function(room, itCb) {
					room.remove(function(error) {
						itCb(error);
					});
				}, function(error) {
					cb(error);
				});
			}
		],
		function(error) {
			res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
		});
	});
}
//----------------------


var prayerHistoryOptions = {
	query: {
		'prayerId': {
			isOptional: false
		},
		'participantId': {
			isOptional: false
		}
	}
};

router.get("/prayerHistory", RequestChecker.requestChecker(prayerHistoryOptions), function(req, res) {	
	async.waterfall([
		function(cb) {
			Room.findByPrayerAndParticipant(req.query.prayerId, req.query.participantId, cb);			
		},
		function(rooms, cb) {
			processRooms(rooms, cb);
		}
	],
	function(error, rooms) {
		res.normSend(error ? error.status || 500 : 0, rooms, error ? error.message : null);		
	});
});


var historyOptions = {
	query: {
		'participantId': {
			isOptional: false
		}
	}
};

router.get("/history", RequestChecker.requestChecker(historyOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			Room.findByParticipantId(req.query.participantId, cb);
		},
		function(rooms, cb) {
			processRooms(rooms, cb);
		}
	],
	function(error, rooms) {
		res.normSend(error ? error.status || 500 : 0, rooms, error ? error.message : null);		
	});
});


router.get("/:roomId", function(req, res) {
	async.waterfall([
		function(cb) {
			Room.findByRoomId(req.params.roomId, cb);
		},
		function(room, cb) {
			processSingleRoom(room, cb);
		}
	],
	function(error, room) {
		res.normSend(error ? error.status || 500 : 0, room, error ? error.message : null);		
	});
});


router.get("/:roomId/info", function(req, res) {
	async.waterfall([
		function(cb) {
			Room.roomInfoByRoomId(req.params.roomId, cb);
		},
		function(room, cb) {
			processSingleRoom(room, cb);
		}
	],
	function(error, room) {
		res.normSend(error ? error.status || 500 : 0, room, error ? error.message : null);		
	});
});


router.get("/:roomId/messages", function(req, res) {	
	Room.messagesByRoomId(req.params.roomId, function(error, messages) {
		res.normSend(error ? error.status || 500 : 0, messages, error ? error.message : null);
	});
});


var sendMessageOptions = {
	body: {
		'roomId': {
			isOptional: true
		},
		'prayerId': {
			isOptional: true
		},
		'receiverId': {
			isOptional: true
		},
		'senderId': {
			isOptional: false
		},
		'date': {
			isOptional: false
		},
		'text': {
			isOptional: false
		}
	}
};

router.post("/message", RequestChecker.requestChecker(sendMessageOptions), function(req, res) {
	
	// validation
	var error = null;
	if(!req.body.roomId)
	{
		if(!req.body.prayerId)
		{
			error = new Error("prayerId is required");
			error.status = 400;
		}
		if(!req.body.receiverId)
		{
			error = new Error("receiverId is required");
			error.status = 400;
		}
	}
	if(!req.body.prayerId)
	{
		if(!req.body.roomId)
		{
			error = new Error("roomId or prayerId is required");
			error.status = 400;
		}		
	}
	if(error)
	{
		res.normSend(error.status || 500, null, error.message);
		return;
	}
	
	async.waterfall([
		function(cb) {
			if(req.body.roomId)
			{
				// Room.findByRoomId(req.body.roomId, cb);

					// -------------------------
					// small piece of shit : BEGIN
					async.waterfall([
						function(pcb) {
							Room.findByRoomId(req.body.roomId, pcb);
						},
						function(room, pcb) {
							Prayer.infoByPrayerId(room.prayerId, function(error, prayer) {
								pcb(error, room, prayer);
							});
						},
						function(room, prayer, pcb) {
							var receiver = prayer.participantByUserIdSync(req.body.receiverId);
							var sender = prayer.participantByUserIdSync(req.body.senderId);
							// console.log("receiver: ", receiver);
							// console.log("sender: ", sender);
							if((!receiver) || (receiver && receiver.status != 'active') || (!sender) || (sender && sender.status != 'active'))
							{
								var error = new Error("One of participant leave prayer");
								error.status = 1001;
								pcb(error, null);		
							}
							else
							{
								pcb(null, room);
							}
						}
					],
					function(error, room) {
						cb(error, error ? null : room);
					});
					// small piece of shit : END
					// -------------------------
			}
			else	// if no room -> create it
			{
				var participantIds = [req.body.receiverId, req.body.senderId];
				Room.createRoom(req.body.prayerId, participantIds, cb);
			}
		},
		function(room, cb) {
			var date = moment(req.body.date, 'YYYY-MM-DDTHH:mm:ss.Z').toDate();
			room.sendMessage(req.body.senderId, date, req.body.text, cb);
		},
	],
	function(error, room) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});	
});


var createRoomOptions = {
	body: {
		'prayerId': {
			isOptional: false
		},
		'receiverId': {
			isOptional: false
		},
		'senderId': {
			isOptional: false
		}
	}
};

router.post("/room", RequestChecker.requestChecker(createRoomOptions), function(req, res) {	
	async.waterfall([
		function(cb) {
			var participantIds = [req.body.receiverId, req.body.senderId];
			Room.createRoom(req.body.prayerId, participantIds, cb);
		},
		function(room, cb) {
			processSingleRoom(room, cb);
		}
	],
	function(error, room) {
		res.normSend(error ? error.status || 500 : 0, room, error ? error.message : null);
	});
});
