var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var moment = require('moment');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var Spot = require('../../models/spot');
var VisitedSpot = require('../../models/visitedSpot');
var ReportedSpot = require('../../models/reportedSpot');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;


router.get("/", function(req, res) {
	Spot.all(function(error, spots) {
		res.normSend(error ? error.status || 500 : 0, spots, error ? error.message : null);
	});
});


var nearestOptions = {
	query: {
		'lat': {
			isOptional: false
		},
		'lon': {
			isOptional: false
		},
		'radius': {
			isOptional: false
		}
	}
};

router.get("/nearestSpots", RequestChecker.requestChecker(nearestOptions), function(req, res) {
	var latitude = req.query.lat;
	var longitude = req.query.lon;
	var radius = req.query.radius;	
	Spot.nearestSpots(latitude, longitude, radius, function(error, spots) {
		res.normSend(error ? error.status || 500 : 0, spots, error ? error.message : null);
	});
});


router.get("/reviewer/:reviewerId", function(req, res) {
	Spot.findByReviewerId(req.params.reviewerId, function(error, spots) {
		res.normSend(error ? error.status || 500 : 0, spots, error ? error.message : null);
	});
});


router.get("/owner/:ownerId", function(req, res) {
	Spot.findByOwnerId(req.params.ownerId, function(error, spots) {
		res.normSend(error ? error.status || 500 : 0, spots, error ? error.message : null);
	});
});


router.get("/reports", function(req, res) {
	ReportedSpot.all(function(error, reportedSpots) {
		res.normSend(error ? error.status || 500 : 0, reportedSpots, error ? error.message : null);
	});
});


router.get("/reports/reporter/:reporterId", function(req, res) {
	ReportedSpot.findByReporterId(req.params.reporterId, function(error, reportedSpots) {
		res.normSend(error ? error.status || 500 : 0, reportedSpots, error ? error.message : null);
	});
});


router.get("/reports/spot/:spotId", function(req, res) {
	ReportedSpot.findBySpotId(req.params.spotId, false, function(error, reportedSpot) {
		res.normSend(error ? error.status || 500 : 0, reportedSpot, error ? error.message : null);
	});
});


router.get("/reports/:reportedSpotId", function(req, res) {
	ReportedSpot.findByReportedSpotId(req.params.reportedSpotId, function(error, reportedSpot) {
		res.normSend(error ? error.status || 500 : 0, reportedSpot, error ? error.message : null);
	});
});


var reportOptions = {
	body: {
		'spotId': {
			isOptional: false
		},
		'reporterId': {
			isOptional: false
		},
		'reason': {
			isOptional: false
		}
	}
};

router.post("/reports", RequestChecker.requestChecker(reportOptions), function(req, res) {	
	ReportedSpot.reportSpot(req.body.spotId, req.body.reporterId, req.body.reason, function(error, reportedSpot) {
		res.normSend(error ? error.status || 500 : 0, !error ? reportedSpot : null, error ? error.message : null);
	});
});


router.delete("/reports/:reportedSpotId", function(req, res) {
	ReportedSpot.removeReportedSpot(req.params.reportedSpotId, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


router.get("/:spotId", function(req, res) {
	Spot.findBySpotId(req.params.spotId, function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, spot, error ? error.message : null);		
	});
});


router.get("/:spotId/visitor/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(req.params.spotId, function(error, spot) {
				cb(error, spot);
			});
		},
		function(spot, cb) {
			VisitedSpot.findBySpotAndVisitor(req.params.spotId, req.params.userId, function(error, visitedSpot) {
				cb(error, spot, visitedSpot);
			});
		},
		function(spot, visitedSpot, cb) {
			// convert Mongoose Object into JSON object before add new fields
			spot = spot.toJSON();
			spot.wasVisited = visitedSpot ? true : false;
			cb(null, spot);
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, spot, error ? error.message : null);		
	});
});


var spotCreateOptions = {
	body: {
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: false
		},
		'description': {
			isOptional: true
		},
		'ownerId': {
			isOptional: false
		}
	}
};

router.post("/", RequestChecker.requestChecker(spotCreateOptions), function(req, res) {	
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var address = req.body.address;
	var description = req.body.description;
	var ownerId = req.body.ownerId;
	Spot.createSpot(latitude, longitude, address, description, ownerId, function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, spot, error ? error.message : null);
	});
});


router.delete("/:spotId", function(req, res) {
	Spot.removeSpot(req.params.spotId, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


var locationOptions = {
	body: {
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: true
		}
	}
};

router.post("/:spotId/location", RequestChecker.requestChecker(locationOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(req.params.spotId, cb);
		},
		function(spot, cb) {
			spot.updateLocation(req.body.latitude, req.body.longitude, req.body.address, cb);	
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


var descriptionOptions = {
	body: {
		'description': {
			isOptional: false
		}
	}
};

router.post("/:spotId/description", RequestChecker.requestChecker(descriptionOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(req.params.spotId, cb);
		},
		function(spot, cb) {
			spot.updateDescription(req.body.description, cb);	
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, error ? null : spot, error ? error.message : null);
	});
});


router.post("/:spotId/photo", function(req, res) {
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				if(!error && !files.photo)
				{
					error = new Error("Photo wasn't provided");
					error.status = 400;
				}
				cb(error, files.photo);
			});
		},
		function(photo, cb) {
			console.log("photo: ", photo);
			Spot.findBySpotId(req.params.spotId, function(error, spot) {
				cb(error, spot, photo);
			});
		},
		function(spot, photo, cb) {
			spot.updatePhoto(photo, cb);
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);		
	});
});


var addReviewOptions = {
	body: {
		'noiseRate': {
			isOptional: false
		},
		'crowdRate': {
			isOptional: false
		},
		'cleanRate': {
			isOptional: false
		},
		'locationRate': {
			isOptional: false
		},
		'comment': {
			isOptional: true
		},
		'reviewerId': {
			isOptional: false
		}
	}
};

router.post("/:spotId/review", RequestChecker.requestChecker(addReviewOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(req.params.spotId, cb);
		},
		function(spot, cb) {
			spot.addReview(req.body.noiseRate, req.body.crowdRate, req.body.cleanRate, req.body.locationRate, req.body.comment || null, req.body.reviewerId, cb);	
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


router.delete("/:spotId/review/:reviewerId", function(req, res) {
	async.waterfall([
		function(cb) {
			Spot.findBySpotId(req.params.spotId, cb);
		},
		function(spot, cb) {
			spot.removeReview(req.params.reviewerId, cb);	
		}
	],
	function(error, spot) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


router.post("/:spotId/visit/:userId", function(req, res) {
	VisitedSpot.visitSpot(req.params.spotId, req.params.userId, function(error, visitedSpot) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);		
	});			
});


router.get("/visited/:userId", function(req, res) {
	async.waterfall([
		function(cb) {
			VisitedSpot.findByVisitorId(req.params.userId, function(error, visitedSpots) {
				cb(error, visitedSpots);
			});			
		},
		function(visitedSpots, cb) {
			var ids = visitedSpots.map(function(element) {return element.spot});
			Spot.findBySpotIds(ids, cb);
		}
	],
	function(error, spots) {
		res.normSend(error ? error.status || 500 : 0, spots, error ? error.message : null);		
	});
});
