var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var Admin = require('../../models/admin');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;

router.get("/", function(req, res) {
	Admin.all(function(error, admins) {
		res.normSend(error ? error.status || 500 : 0, admins, error ? error.message : null);
	});
});


router.get("/usernamemany/:username", function(req, res) {
	Admin.findLocationManagersByUsername(req.params.username, function(error, admins) {
		res.normSend(error ? error.status || 500 : 0, admins, error ? error.message : null);
	});
});


router.get("/allmanagers", function(req, res) {
	Admin.findLocationManagersByUsername('', function(error, admins) {
		res.normSend(error ? error.status || 500 : 0, admins, error ? error.message : null);
	});
});


router.get("/:adminId", function(req, res) {
	Admin.findByAdminId(req.params.adminId, function(error, admin) {
		res.normSend(error ? error.status || 500 : 0, admin, error ? error.message : null);
	});
});


var adminCreateOptions = {
	body: {
		'username': {
			isOptional: false
		},
		'password': {
			isOptional: false
		},
		'email': {
			isOptional: false
		},
		'role': {
			isOptional: false
		}
	}
};

router.post("/", RequestChecker.requestChecker(adminCreateOptions), function(req, res) {
	Admin.createAdmin(req.body.username, req.body.password, decodeURIComponent(req.body.email), req.body.role, function(error, admin) {
		res.normSend(error ? error.status || 500 : 0, admin, error ? error.message : null);
	});
});

router.delete("/:adminId", function(req, res) {
	Admin.removeAdmin(req.params.adminId, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});
