var express = require('express');
var router = express.Router();
var path = require('path');

router.get("/:name", function(req, res){
	console.log("name of the photo: ", req.params.name);
	var options = {
	    root: path.join(__dirname, '../public/images'),
	    dotfiles: 'deny',
	    headers: {
	        'x-timestamp': Date.now(),
	        'x-sent': true
	    }
	  };
	var fileName = req.params.name;
	res.sendFile(fileName, options, function(error) {
		if(error)
		{
			console.log(error);
			res.end();
		}
	});
});

module.exports = router;
