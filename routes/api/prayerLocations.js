var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var moment = require('moment');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var PrayerLocation = require('../../models/prayerLocation');
var Admin = require('../../models/admin');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;


router.get("/", function(req, res) {
	PrayerLocation.all(function(error, prayerLocations) {
		res.normSend(error ? error.status || 500 : 0, prayerLocations, error ? error.message : null);
	});
});


var nearestOptions = {
	query: {
		'lat': {
			isOptional: false
		},
		'lon': {
			isOptional: false
		},
		'radius': {
			isOptional: false
		}
	}
};

router.get("/nearestLocations", RequestChecker.requestChecker(nearestOptions), function(req, res) {
	var latitude = req.query.lat;
	var longitude = req.query.lon;
	var radius = req.query.radius;	
	PrayerLocation.nearestLocations(latitude, longitude, radius, function(error, prayerLocations) {
		res.normSend(error ? error.status || 500 : 0, prayerLocations, error ? error.message : null);
	});
});


router.get("/all-for-map", function(req, res) {
	PrayerLocation.find().select("id location name placeType").exec(function(error, prayerLocations) {
		res.normSend(error ? error.status || 500 : 0, prayerLocations, error ? error.message : null);
	});
});


router.get("/:prayerLocationId", function(req, res) {
	PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, prayerLocation, error ? error.message : null);
	});
});


var locationCreateOptions = {
	body: {
		'name': {
			isOptional: false
		},
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: false
		},
		'placeType': {
			isOptional: false
		},
		'timeOpen': {
			isOptional: true,
			allowEmpty: true
		},
		'timeClose': {
			isOptional: true,
			allowEmpty: true
		},
		'alwaysOpen': {
			isOptional: true
		},
		'timeFajrStart': {
			isOptional: true
		},
		'timeFajrIqamah': {
			isOptional: true
		},
		'timeDhuhrStart': {
			isOptional: true
		},
		'timeDhuhrIqamah': {
			isOptional: true
		},
		'timeAsrStart': {
			isOptional: true
		},
		'timeAsrIqamah': {
			isOptional: true
		},
		'timeMaghribStart': {
			isOptional: true
		},
		'timeMaghribIqamah': {
			isOptional: true
		},
		'timeIshaStart': {
			isOptional: true
		},
		'timeIshaIqamah': {
			isOptional: true
		},
		'managerUsername': {
			isOptional: false
		}
	}
};

router.post("/", RequestChecker.requestChecker(locationCreateOptions), function(req, res) {	
	var name = req.body.name;
	var latitude = req.body.latitude;
	var longitude = req.body.longitude;
	var address = req.body.address;
	var placeType = req.body.placeType;
	var timeOpen = req.body.timeOpen;
	var timeClose = req.body.timeClose;
	var alwaysOpen = req.body.alwaysOpen;
	var timeFajrStart = req.body.timeFajrStart;
	var timeFajrIqamah = req.body.timeFajrIqamah;
	var timeDhuhrStart = req.body.timeDhuhrStart;
	var timeDhuhrIqamah = req.body.timeDhuhrIqamah;
	var timeAsrStart = req.body.timeAsrStart;
	var timeAsrIqamah = req.body.timeAsrIqamah;
	var timeMaghribStart = req.body.timeMaghribStart;
	var timeMaghribIqamah = req.body.timeMaghribIqamah;
	var timeIshaStart = req.body.timeIshaStart;
	var timeIshaIqamah = req.body.timeIshaIqamah;
	var managerUsername = req.body.managerUsername;
	
	PrayerLocation.createPrayerLocation(
		name,
		latitude, longitude, address,
		placeType,
		timeOpen,
		timeClose,
		alwaysOpen,
		timeFajrStart,
		timeFajrIqamah,
		timeDhuhrStart,
		timeDhuhrIqamah,
		timeAsrStart,
		timeAsrIqamah,
		timeMaghribStart,
		timeMaghribIqamah,
		timeIshaStart,
		timeIshaIqamah,
		managerUsername,
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, prayerLocation.id, error ? error.message : null);
	});
});


router.delete("/:prayerLocationId", function(req, res) {
	PrayerLocation.removePrayerLocation(req.params.prayerLocationId, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


var locationOptions = {
	body: {
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		},
		'address': {
			isOptional: true
		}
	}
};

router.post("/:prayerLocationId/location", RequestChecker.requestChecker(locationOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});			
		},
		function(prayerLocation, cb) {
			prayerLocation.updateLocation(req.body.latitude, req.body.longitude, req.body.address, cb);	
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.id, error ? error.message : null);		
	});
});


router.post("/:prayerLocationId/photo", function(req, res) {
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				if(!error && !files.photo)
				{
					error = new Error("Photo wasn't provided");
					error.status = 400;
				}
				cb(error, files.photo);
			});
		},
		function(photo, cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation, photo);
			});			
		},
		function(prayerLocation, photo, cb) {
			prayerLocation.updatePhotoFile(photo, cb);	
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.id, error ? error.message : null);		
	});
});

router.post("/:prayerLocationId/general", function(req, res) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});
		},
		function(prayerLocation, cb) {
			Admin.findByUsername(req.body.managerUsername, true, function(error, manager) {
				cb(error, prayerLocation, manager);
			});
		},
		function(prayerLocation, manager, cb) {
			prayerLocation.name = req.body.name;
			// prayerLocation.location = {
			// 	coordinate:[req.body.longitude, req.body.latitude],
			// 	address: req.body.address
			// };
			prayerLocation.placeType = req.body.placeType;
			prayerLocation.alwaysOpen = req.body.alwaysOpen;
			if(req.body.alwaysOpen === true)
			{
				prayerLocation.timeOpen = null;
				prayerLocation.timeClose = null;
			}
			else
			{
				prayerLocation.timeOpen = req.body.timeOpen;
				prayerLocation.timeClose = req.body.timeClose;
			}
			prayerLocation.managerUsername = req.body.managerUsername;

			prayerLocation.updateLocation(req.body.latitude, req.body.longitude, req.body.address, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.id, error ? error.message : null);
	});
});


router.get("/:prayerLocationId/photo", function(req, res) {
	PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.photo, error ? error.message : null);		
	});			
});


router.delete("/:prayerLocationId/photo", function(req, res) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});			
		},
		function(prayerLocation, cb) {
			prayerLocation.updatePhoto(null, cb);
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.photo, error ? error.message : null);		
	});
});


router.post("/:prayerLocationId/mosquelogo", function(req, res) {
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				if(!error && !files.photo)
				{
					error = new Error("Photo wasn't provided");
					error.status = 400;
				}
				cb(error, files.photo);
			});
		},
		function(photo, cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation, photo);
			});			
		},
		function(prayerLocation, photo, cb) {
			prayerLocation.updateMosqueLogoFile(photo, cb);	
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.id, error ? error.message : null);		
	});
});


router.get("/:prayerLocationId/mosquelogo", function(req, res) {
	PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.mosqueLogo, error ? error.message : null);		
	});			
});


router.delete("/:prayerLocationId/mosquelogo", function(req, res) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});			
		},
		function(prayerLocation, cb) {
			prayerLocation.updateMosqueLogo(null, cb);
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.mosqueLogo, error ? error.message : null);		
	});
});


router.post("/:prayerLocationId/prayertimes", function(req, res) {
	async.waterfall([
		function(cb) {
			PrayerLocation.findByPrayerLocationId(req.params.prayerLocationId, function(error, prayerLocation) {
				cb(error, prayerLocation);
			});
		},
		function(prayerLocation, cb) {
			prayerLocation.timeFajrStart = req.body.timeFajrStart;
			prayerLocation.timeFajrIqamah = req.body.timeFajrIqamah;
			prayerLocation.timeDhuhrStart = req.body.timeDhuhrStart;
			prayerLocation.timeDhuhrIqamah = req.body.timeDhuhrIqamah;
			prayerLocation.timeAsrStart = req.body.timeAsrStart;
			prayerLocation.timeAsrIqamah = req.body.timeAsrIqamah;
			prayerLocation.timeMaghribStart = req.body.timeMaghribStart;
			prayerLocation.timeMaghribIqamah = req.body.timeMaghribIqamah;
			prayerLocation.timeIshaStart = req.body.timeIshaStart;
			prayerLocation.timeIshaIqamah = req.body.timeIshaIqamah;
			
			prayerLocation.save(function(error, prayerLocation) {
				cb(error, prayerLocation);
			});
		}
	],
	function(error, prayerLocation) {
		res.normSend(error ? error.status || 500 : 0, error ? null : prayerLocation.id, error ? error.message : null);
	});
});
