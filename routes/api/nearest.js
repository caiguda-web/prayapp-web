var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var Prayer = require('../../models/prayer');
var User = require('../../models/user');
var PrayerLocation = require('../../models/prayerLocation');
var Spot = require('../../models/spot');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;

var nearestOptions = {
	query: {
		'lat': {
			isOptional: false
		},
		'lon': {
			isOptional: false
		},
		'radius': {
			isOptional: false
		},
		'sect': {
			isOptional: false
		},
		'prayerType': {
			isOptional: true
		}
	}
};

router.get("/", RequestChecker.requestChecker(nearestOptions), function(req, res) {
	var latitude = req.query.lat;
	var longitude = req.query.lon;
	var radius = req.query.radius;
	var sect = req.query.sect;
	var prayerType = req.query.prayerType;
	var showOnlyEnabledUsers = true;	//typeof(req.query.showOnlyEnabled) == 'undefined' ? false : req.query.showOnlyEnabled;

	async.waterfall([
		function(cb) {
			User.nearestUsers(sect, latitude, longitude, radius, showOnlyEnabledUsers, cb);
		},
		function(users, cb) {
			Prayer.nearestPrayers(prayerType, sect, latitude, longitude, radius, function(error, prayers) {
				cb(error, users, prayers);
			});
		},
		function(users, prayers, cb) {
			PrayerLocation.nearestLocations(latitude, longitude, radius, function(error, prayerLocations) {
				cb(error, users, prayers, prayerLocations);
			});
		},
		function(users, prayers, prayerLocations, cb) {
			Spot.nearestSpots(latitude, longitude, radius, function(error, spots) {
				cb(error, users, prayers, prayerLocations, spots);
			});
		}
	],
	function(error, users, prayers, prayerLocations, spots) {
		var data = {
			users: users,
			prayers: prayers,
			locations: prayerLocations,
			spots: spots
		};
		res.normSend(error ? error.status || 500 : 0, error ? null : data, error ? error.message : null);
	});
});

