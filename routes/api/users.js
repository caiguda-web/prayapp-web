var express = require('express');
var router = express.Router();
var util = require('util');
var async = require('async');
var Formidable = require("formidable");
var RequestChecker = require('express-request-checker');
var logger = require('../../libs/logger');
var config = require('config');
var User = require('../../models/user');

RequestChecker.setHttpResponseBodyErrorFormat(
	JSON.stringify(
		{
			code: 400,
			data: null,
			error: {
				reporter    : "express-request-checker",
				scope       : "`{{scope}}`",
				field       : "`{{field}}`",
				message     : "{{errorMessage}}",
				errorDetail : "{{errorDetail}}"
			}
		}
	)
);

module.exports = router;

router.get("/", function(req, res) {
	User.all(function(error, users) {
		res.normSend(error ? error.status || 500 : 0, users, error ? error.message : null);
	});
});


router.get("/nearestUsers", function(req, res) {
	var latitude = req.query.lat;
	var longitude = req.query.lon;
	var radius = req.query.radius;
	var sect = req.query.sect;
	var showOnlyEnabledUsers = false;	//typeof(req.query.showOnlyEnabled) == 'undefined' ? false : req.query.showOnlyEnabled;
	
	User.nearestUsers(sect, latitude, longitude, radius, showOnlyEnabledUsers, function(error, users) {
		res.normSend(error ? error.status || 500 : 0, error ? null : users, error ? error.message : null);
	});
});


router.get("/:userId", function(req, res) {
	User.findByUserId(req.params.userId, function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});
});


router.get("/social/:socialId", function(req, res) {
	User.findBySocialId(req.params.socialId, true, function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});
});


router.post("/signupWithPhoto", function(req, res) {	
	// console.log("signup");
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				cb(error, fields, files);
			});
		},
		function(fields, files, cb) {			
			User.signup(fields.email ? decodeURIComponent(fields.email) : null, 
									fields.password,
									fields.name,
									fields.surname,
									fields.gender,
									fields.sect,
									files.avatar, 
									fields.socialIdentifier,
									fields.socialType,
			function(error, user) {
				cb(error, user);
			});
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});
});


// POST users/signup
var signupOptions = {
	body: {
		'email': {
			isOptional: true
		},
		'password': {
			isOptional: true
		},
		'name': {
			isOptional: false			
		},
		'surname': {
			isOptional: false			
		},
		'gender': {
			isOptional: false			
		},
		'sect': {
			isOptional: false			
		},
		'socialIdentifier': {
			isOptional: true			
		},
		'socialType': {
			isOptional: true			
		},
	}
};

router.post("/signup", RequestChecker.requestChecker(signupOptions), function(req, res) {
	User.signup(req.body.email ? decodeURIComponent(req.body.email) : null, 
							req.body.password,
							req.body.name,
							req.body.surname,
							req.body.gender,
							req.body.sect,
							null,
							req.body.socialIdentifier,
							req.body.socialType,
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});
});


//*
// POST users/signin
var signinOptions = {
	body: {
		'email': {
			isOptional: false
		},
		'password': {
			isOptional: false
		}
	}
};
//*/

// router.post("/signin", function(req, res) {
router.post("/signin", RequestChecker.requestChecker(signinOptions), function(req, res) {
	// console.log("SIGNIN");
	// console.log(":", req.body);
	User.signin(req.body.email, req.body.password, function(error, user) {
		// console.log("=", error);
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});	
});


router.post("/signin/:socialId", function(req, res) {
	User.signinBySocialId(req.params.socialId, function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);
	});	
});


router.post("/:userId/signout", function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, cb);
		},
		function(user, cb) {
			user.signout(cb);	
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);		
	});
});


router.delete("/:userId", function(req, res) {
	User.removeUser(req.params.userId, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});


// POST users/restore
var profileOptions = {
	body: {
		'name': {
			isOptional: false
		},
		'surname': {
			isOptional: false
		},
		'gender': {
			isOptional: false
		},
		'sect': {
			isOptional: false
		},
		'prayReminder': {
			isOptional: false
		},
		'searchRadius': {
			isOptional: false
		},
		'messagesNotificationEnabled': {
			isOptional: false
		},
		'showOnTheMap': {
			isOptional: true
		}
	}
};

router.post("/:userId/profile", RequestChecker.requestChecker(profileOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, cb);
		},
		function(user, cb) {
			var showOnTheMap = typeof(req.body.showOnTheMap) == 'undefined' ? false : req.body.showOnTheMap;
			user.updateProfile( req.body.name, 
												  req.body.surname,
													req.body.gender,
													req.body.sect,
													req.body.prayReminder,
					 								req.body.searchRadius, 
													req.body.messagesNotificationEnabled,
													showOnTheMap,
													cb);	
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);		
	});
});


// POST users/password
var passwordOptions = {
	body: {
		'password': {
			isOptional: false
		}
	}
};

router.post("/:userId/password", RequestChecker.requestChecker(passwordOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, cb);
		},
		function(user, cb) {
			user.updatePassword(req.body.password, cb);	
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, user, error ? error.message : null);		
	});
});


// POST users/password
var locationOptions = {
	body: {
		'latitude': {
			isOptional: false
		},
		'longitude': {
			isOptional: false
		}
	}
};

router.post("/:userId/location", RequestChecker.requestChecker(locationOptions), function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, function(error, user) {
				cb(error, user);
			});			
		},
		function(user, cb) {
			user.updateLocation(req.body.latitude, req.body.longitude, cb);	
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);		
	});
});


router.post("/:userId/avatar", function(req, res) {
	async.waterfall([
		function(cb) {
			var form = new Formidable.IncomingForm();
			form.keepExtensions = true;
			form.parse(req, function(error, fields, files) {
				if(!error && !files.avatar)
				{
					error = new Error("Avatar wasn't provided");
					error.status = 400;
				}
				cb(error, files.avatar);
			});
		},
		function(avatar, cb) {
			User.findByUserId(req.params.userId, function(error, user) {
				cb(error, user, avatar);
			});			
		},
		function(user, avatar, cb) {
			user.updateAvatar(avatar, cb);	
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, error ? null : user, error ? error.message : null);		
	});
});


// POST users/restore
var restoreOptions = {
	body: {
		'email': {
			isOptional: false
		}
	}
};

router.post("/restore", RequestChecker.requestChecker(restoreOptions), function(req, res) {
	User.restore(req.body.email, function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});	
});


router.post("/:userId/push/:pushToken/:tokenType", function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, cb);
		},
		function(user, cb) {
			user.registerPushToken(req.params.pushToken, req.params.tokenType, cb);
		}
	],
	function(error, user) {
		res.normSend(error ? error.status || 500 : 0, error ? null : user, error ? error.message : null);
	});
});



// test push notification
var apn = require('../../libs/apn');
router.get("/:userId/testpush", function(req, res) {
	async.waterfall([
		function(cb) {
			User.findByUserId(req.params.userId, cb);
		},
		function(user, cb) {
			var notification = {};
			notification.alert = "Test push notification";
			notification.sound = "default";
			notification.payload = {
				type: "invitation"
			};
			// notification.clickAction = "invitation";
			user.pushNotification(notification);
			cb(null);
		}
	],
	function(error) {
		res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
	});
});

// only for development
if (config.util.getEnv('NODE_ENV') === 'development') {
	router.post("/all/cleartokens", function(req, res) {
		async.waterfall([
			function(cb) {
				User.all(cb);
			},
			function(users, cb) {
				async.each(users, function(user, itCb) {
					user.clearPushTokens(itCb);
				}, function(error) {
					cb(error);
				});
			}
		],
		function(error) {
			res.normSend(error ? error.status || 500 : 0, null, error ? error.message : null);
		});
	});

	router.post("/:userId/cleartokens", function(req, res) {
		async.waterfall([
			function(cb) {
				User.findByUserId(req.params.userId, cb);
			},
			function(user, cb) {
				user.clearPushTokens(cb);
			}
		],
		function(error, user) {
			res.normSend(error ? error.status || 500 : 0, error ? null : user, error ? error.message : null);
		});
	});
}
